import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { FavoritesPage } from '../pages/favorites/favorites';
import { OrdersPage } from '../pages/orders/orders';
import { ProfilePage } from '../pages/profile/profile';
import { CheckoutPage } from '../pages/checkout/checkout';
import { OrderhistoryPage } from '../pages/orderhistory/orderhistory';
import { ImagemapPage } from '../pages/imagemap/imagemap';
import { VendingmachinePage } from '../pages/vendingmachine/vendingmachine';
import { GroceryPage } from '../pages/grocery/grocery';
import { ConfirmorderPage } from '../pages/confirmorder/confirmorder';
import { MyinfoPage } from '../pages/myinfo/myinfo';
import { MypointsPage } from '../pages/mypoints/mypoints';
import { AddressPage } from '../pages/address/address';
import { FriendsPage } from '../pages/friends/friends';
import { GiftsPage } from '../pages/gifts/gifts';
import { ProductPage } from '../pages/product/product';
import { ReviewsPage } from '../pages/reviews/reviews';
import { QaPage } from '../pages/qa/qa';
import { StorePage } from '../pages/store/store';
import { MypromosPage } from '../pages/mypromos/mypromos';
import { CategoriesPage } from '../pages/categories/categories'
import { AuthPage } from '../pages/auth/auth';
import { ContactusPage } from '../pages/contactus/contactus';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DataserviceProvider } from '../providers/dataservice/dataservice';
import { SalePage } from '../pages/sale/sale';
import { DragulaModule } from '../../node_modules/ng2-dragula/ng2-dragula';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { NetworkEngineProvider } from '../providers/network-engine/network-engine';
import { IonicStorageModule } from '@ionic/storage';

import { Geolocation } from '@ionic-native/geolocation';
import {PlacePage} from "../pages/place/place";
import {HTTP} from "@ionic-native/http";
import {SearchaddPage} from "../pages/searchadd/searchadd";

import { Network } from '@ionic-native/network';
import { LongPressModule } from 'ionic-long-press';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    FavoritesPage,
    OrdersPage,
    ProfilePage,
    CheckoutPage,
    OrderhistoryPage,
    ImagemapPage,
    VendingmachinePage,
    GroceryPage,
    ConfirmorderPage,
    MyinfoPage,
    MypointsPage,
    AddressPage,
    FriendsPage,
    GiftsPage,
    ProductPage,
    ReviewsPage,
    QaPage,
    StorePage,
    MypromosPage,
    SalePage,
    CategoriesPage,
    AuthPage,
    ContactusPage,
    PlacePage,
    SearchaddPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    DragulaModule,
    LongPressModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    FavoritesPage,
    OrdersPage,
    ProfilePage,
    CheckoutPage,
    OrderhistoryPage,
    ImagemapPage,
    VendingmachinePage,
    GroceryPage,
    ConfirmorderPage,
    MyinfoPage,
    MypointsPage,
    AddressPage,
    FriendsPage,
    GiftsPage,
    ProductPage,
    ReviewsPage,
    QaPage,
    StorePage,
    MypromosPage,
    SalePage,
    CategoriesPage,
    AuthPage,
    ContactusPage,
    PlacePage,
    SearchaddPage
  ],
  providers: [
    Network,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClientModule,
    HttpClient,
    HTTP,
    Geolocation,
    DataserviceProvider,
    NetworkEngineProvider
  ]
})
export class AppModule {}
