import { Component, OnInit } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {HttpClient} from "@angular/common/http";
import {AddressPage} from "../address/address";
import {ProfilePage} from "../profile/profile";
import {Network} from "@ionic-native/network";

/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage implements OnInit{

  friends = [];


  openModal = false;
  mobileReq:any;
  openFriend = false;
  phone:any;

  customer_id:any;
  firstname:any;
  lastname:any;
  fullname:any;
  mobile:any;
  email:any;
  noResult = false;

  reqFn:any;
  reqFirstname:any;
  reqLastname:any;
  reqCustomerid:any;
  reqMobile:any;
  reqEmail:any;

  friendsReq:any;
  friendsReqCount:any;
  friend_lists:any;
  friend_lists_count:any;
  login_success:any;

  f_r:any;
  f_l:any;

  warning:any;
  location_warning:any;
  friend_alert:any;
  friend_alert_message:any;
  array_friend = [];


  constructor(private alertCtrl: AlertController, public navCtrl: NavController, private storage: Storage, public navParams: NavParams, private data: DataserviceProvider, private http: HttpClient, public network: Network) {

  }

  ionViewDidLoad() {

    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidLoad FriendsPage');
    console.log("==========================");
    this.checkLogin();

  }

  FriendAlert() {
    this.friend_alert = this.alertCtrl.create({
      title: 'Friend Request',
      message: this.friend_alert_message,
      buttons: [

        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    this.friend_alert.present();
  }



  checkLogin(){
    this.storage.get("success").then( log => {
      if(log == 1){
        console.log("LOGGED ON");

        this.storage.get('customer_id').then((customer_id) => {
          console.log("USER ID:",customer_id);
          this.customer_id = customer_id;
          this.getAccountInfo();

          // this.getFriendRequests();

          // this.viewFriendsRequests();
          // this.viewFriendLists();

        });
      }else{
        console.log("LOGGED OUT");
      }

    })
  }

  getAccountInfo() {

    this.storage.get('firstname').then((firstname) => {
      this.firstname = firstname;
      this.storage.get('lastname').then((lastname) => {
        this.lastname = lastname;
        this.storage.get('email').then((email) => {
          this.fullname = this.firstname + " " + this.lastname;
          this.email = email;
          this.storage.get('mobile').then((mobile) => {
            this.mobile = mobile;

            this.myFriends();
            this.myFriendRequest();

            console.log(this.fullname);
            console.log(this.firstname);
            console.log(this.lastname);
            console.log(this.email);
            console.log(this.mobile);
          });
        });
      });
    });
  }

  /*getFriendLists(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.customer_id)
      .subscribe(fl => {
        this.f_l = fl;
        // this.storage.set('my_friend_lists', fr);
        console.log("FRIEND LISTS STORED.");

        if (this.f_l == null){
          console.log("NO RESULTS");
        }
        else{
          var obj = JSON.parse(this.f_l);


          if (obj.success == 3){
            console.log("NO FRIEND REQUEST!");
          }else if (obj.success == 0){
            console.log("NO ID, NO FRIEND REQUEST");
          }else{
            this.friend_lists = obj.friend_request;
            this.friend_lists_count = obj.friend_request.length;
            console.log("FRIENDS:",fl);
            console.log("FRIENDS COUNT:",fl.friend_request.length);
          }

        }


      });
  }

  getFriendRequests(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friend_request.php?friend_request_to="+this.customer_id)
      .subscribe(fr => {
        // this.storage.set('my_friend_requests', fr);
        this.f_r = fr;
        console.log("FRIEND REQUEST STORED.");

        if (this.f_r == null){
          this.friendsReqCount = null;
          console.log("NO RESULTS");
        }
        else{
          var obj = JSON.parse(this.f_r);

          if (obj.success == 3){
            console.log("NO FRIEND REQUEST!");
          }else if (obj.success == 0){
            console.log("NO ID, NO FRIEND REQUEST");
          }else{
            this.friendsReq = obj.friend_request;
            this.friendsReqCount = obj.friend_request.length;
            console.log("FRIEND REQUESTS:",obj);
            console.log("NUMBER OF FRIEND REQUEST:",obj.friend_request.length);
          }

        }

      });
  }*/

  viewFriendsRequests(){
    this.storage.get('my_friend_requests').then((fr_req) => {

      if (fr_req == null){
        this.friendsReqCount = null;
        console.log("NO RESULTS");
      }
      else{

        if (fr_req.success == 3){
          console.log("NO FRIEND REQUEST!");
        }else if (fr_req.success == 0){
          console.log("NO ID, NO FRIEND REQUEST");
        }else{
          this.friendsReq = fr_req.friend_request;
          this.friendsReqCount = fr_req.friend_request.length;
          console.log("FRIEND REQUESTS:",fr_req);
          console.log("FRIEND REQUEST:",fr_req.friend_request.length);
        }

      }
    });
  }

  viewFriendLists(){
    this.storage.get('my_friend_lists').then((fl) => {

      if (fl == null){
        console.log("NO RESULTS");
      }
      else{

        if (fl.success == 3){
          console.log("NO FRIEND REQUEST!");
        }else if (fl.success == 0){
          console.log("NO ID, NO FRIEND REQUEST");
        }else{


          this.friend_lists = fl.friend_request;
          this.friend_lists_count = fl.friend_request.length;
          console.log("FRIENDS:",fl);
          console.log("FRIENDS COUNT:",fl.friend_request.length);

        }

      }
    });
  }






  deleteReq(fl_id, val, fname, lname){
    console.log("DELETE FRIEND REQUEST FROM: ");
    console.log("friend list id: "+ fl_id);
    console.log("friend request id: "+ val);
    console.log("first name:" + fname);
    console.log("last name: "+ lname)

    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_delete_friend_request.php?friend_list_id="+fl_id)
      .subscribe(data => {
        console.log(data);

        this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friend_request.php?friend_request_to="+this.customer_id)
          .subscribe(data => {
            this.storage.set('my_friend_requests', data);
          });

        let currentIndex = this.navCtrl.getActive().index;
        console.log("current index:",currentIndex);

        /*this.navCtrl.push(FriendsPage)
          .then(() => {
            this.navCtrl.remove(currentIndex);
          });*/

        this.navCtrl.setRoot(ProfilePage).then( () =>{
          this.navCtrl.push(FriendsPage);
        });

      })
  }

  acceptReq(fr_id, val, fname, lname)
  {

    console.log("ACCEPT FRIEND REQUEST FROM: ");
    console.log("friend list id: "+ fr_id);
    console.log("friend request id: "+ val);
    console.log("first name:" + fname);
    console.log("last name: "+ lname)


    console.log("INSERT DATA WHEN ACCEPTED:");
    console.log("friend_request_from:"+ this.customer_id);
    console.log("friend_request_to:"+val);
    console.log("status:"+1);

    console.log("UPDATE DATA:");
    console.log("friend list id:"+ fr_id);
    console.log("status:"+1);

    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_accept_friend_request.php?id="+ fr_id +"&friend_request_from="+ this.customer_id +"&friend_request_to="+val)
      .subscribe(data => {
        console.log("HTTP:");
        console.log(data);

      });


    /*let currentIndex = this.navCtrl.getActive().index;
    console.log("current index:",currentIndex);*/

    /*this.navCtrl.push(FriendsPage)
      .then(() => {
        this.navCtrl.remove(currentIndex);
      });*/

    this.navCtrl.setRoot(ProfilePage).then( () =>{
      this.navCtrl.push(FriendsPage);
    });


  }

  ngOnInit() {
    this.data.currentFriends.subscribe(val => this.friends = val);
  }


  addFriend(){

    if(this.mobileReq == this.mobile){
      this.friend_alert_message = "That is your phone number.";
      this.FriendAlert();
    }else{


    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_add_search_friend.php?mobile="+this.mobileReq)
      .subscribe(data => {

        var str = JSON.stringify(data);
        var obj = JSON.parse(str);

        console.log("Existing Account Retrieve Data:");
        console.log(data);

        if(obj.success == 1){

          this.openModal = false;
          this.openFriend = true;

          this.reqFn = obj.firstname + " " + obj.lastname;
          this.reqCustomerid = obj.customer_id;
          this.reqFirstname = obj.firstname;
          this.reqLastname = obj.lastname;
          this.reqEmail = obj.email;
          this.reqMobile = obj.mobile;

        }else{
          this.openModal = false;
          this.noResult = true;
        }


      })

    }
  }

  friendRequest()
  {

    console.log("from:",this.customer_id);
    console.log("to:",this.reqCustomerid);
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_friend_request_to.php?friend_request_from="+ this.customer_id +"&friend_request_to="+this.reqCustomerid)
      .subscribe(val => {
        console.log(val);
        var str = JSON.stringify(val);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("Currently no friend request to this person");
          this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_send_friend_request.php?friend_list_id=&friend_request_from="+ this.customer_id+"&friend_request_to="+ this.reqCustomerid)
            .subscribe(val => {
              console.log("FRIEND REQUEST SUCCESS:");
              console.log(val);
              this.openFriend = false;
              // alert("Friend Request Sent.");
              this.friend_alert_message = "Friend Request Sent.";
              this.FriendAlert();
            });
        }else {
          // console.log("Either friend already or friend request");
          if (obj.status == 0) {
            // alert("You already sent a friend request.");
            this.friend_alert_message = "You already sent a friend request.";
            this.FriendAlert();
          } else if (obj.status == 1) {
            // alert("You are already friends.");
            this.friend_alert_message = "You are already friends.";
            this.FriendAlert();
          }
        }

      })

  }

  myFriends(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.customer_id)
      .subscribe(fl => {
        var str = JSON.stringify(fl);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("NO RESULT!");
          console.log(obj);
        }else{
          console.log("FRIENDS:",obj);
          this.friend_lists = obj.friend_request;
          this.friend_lists_count = obj.friend_request.length;
          this.storage.set('my_friend_lists', obj);

        }

      });
  }
  myFriendRequest(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friend_request.php?friend_request_to="+this.customer_id)
      .subscribe(fr => {
        var str = JSON.stringify(fr);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("NO RESULT!");
          console.log(obj);
        }else{
          console.log("FRIEND REQUEST:",obj);
          this.friendsReq = obj.friend_request;
          this.storage.set('my_friend_requests', obj);

        }

      });
  }



}
