import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { QaPage } from '../qa/qa';
import { ReviewsPage } from '../reviews/reviews';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage implements OnInit {

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  product:any = null;
  store:any = null;

  heartState = "heart-outline";


  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);

    this.product = this.products.find(p => p.prodId == this.navParams.get("prodId"));
    this.store = this.stores.find(s => s.storeId == this.product.storeId);

    if (this.product.inFavorites) {
      this.heartState = "heart";
    } 
    
  }

  toggleFavorite(id, val) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateFavorites(id, (product.inFavorites = !val));
    if (val) {
      this.heartState = "heart-outline"
    } else {
      this.heartState = "heart"
    }
  }

  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }

  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) { 
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      } 
    }
    this.data.updateCartCount(--this.cartCount);
  }

  goToQA() {
    this.navCtrl.push(QaPage, {
      prodId: this.product.prodId,
      storeId: this.store.storeId
    });
  }

  goToReviews() {
    this.navCtrl.push(ReviewsPage);
  }

}
