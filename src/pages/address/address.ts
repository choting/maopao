

import { Component, OnInit , ViewChild, ElementRef } from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {Storage} from "@ionic/storage";
import { Geolocation } from '@ionic-native/geolocation';
import {HttpClient} from "@angular/common/http";
import {PlacePage} from "../place/place";
import {SearchaddPage} from "../searchadd/searchadd";
import {MyinfoPage} from "../myinfo/myinfo";

/**
 * Generated class for the AddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage implements OnInit{

  // variable for database
    DBaddress_id:any;
    DBcustomer_id:any;
    DBfirstname:any;
    DBlastname:any;
    DBmobile:any
    DBadressType:any
    DBaddress:any;
    DBlandmark:any;
    DBlatitude:any;
    DBlongitude:any;

  // variable for database

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker:any;

  lat: any;
  lng: any;

  latLng: string =  null;

  customer_id:any;

  holderLat:any;
  holderLng:any;

  isChanged:boolean = false;
  isNoAddress:boolean = false;

  addressType = "";
  landmark = "";
  addResidential = "R";
  addOffice = "O";
  addOther = "OTH"

  formattedAddress: any;
  address_id:any;

  address = [];
  selectedAddress = null;
  title = null;

  isMapEnabled:boolean = false;
  mobile:any;
  firstname:any;
  lastname:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private geo: Geolocation, private http: HttpClient, private modalCtrl:ModalController) {
    this.selectedAddress =  this.navParams.get("address");

    storage.get('mobile').then((val) => {
      if(val != null){
        this.mobile = val;
        console.log('Your mobile is', this.mobile);
      }
    });
    storage.get('firstname').then((val) => {
      if(val != null){
        this.firstname = val;
        console.log('Your firstname is ', this.firstname);
      }
    });
    storage.get('lastname').then((val) => {
      if(val != null){
        this.lastname = val;
        console.log('Your lastname is ', this.lastname);
      }
    });

    /*setTimeout(() => {
      storage.get('address_id').then((val) => {
      console.log('Your address id is', val);
      })

      storage.get('mobile').then((val) => {
        console.log('Your mobile is', val);

        if(val != null){
          this.mobile = val;
          console.log('Your mobile is', this.mobile);
        }

      });

      storage.get('firstname').then((val) => {
        console.log('Your firstname is ', val);

        if(val != null){
          this.firstname = val;
          console.log('Your firstname is ', this.firstname);
        }

      })

      storage.get('lastname').then((val) => {
        console.log('Your lastname is ', val);

        if(val != null){
          this.lastname = val;
          console.log('Your lastname is ', this.lastname);
        }

      })

      storage.get('address_type').then((val) => {
        console.log('Your address_type is ', val);

        /!*if(val != null){
          this.addressType = val;
          console.log('Your address type is ', this.addressType);
        }*!/

      })

      storage.get('landmark').then((val) => {
        console.log('Your landmark is ', val);

        /!*if(val != null){
          this.landmark = val;
          console.log('Your landmark is ', this.landmark);
        }*!/

      })

    }, 2500)*/;



    this.storage.get('customer_id').then((val) => {
      // console.log('Your id is', val);
      this.customer_id = val;

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_selected_address.php?customer_id=' + this.customer_id + '&address=' + this.selectedAddress, {})
        .subscribe(data => {
          var str2 = JSON.stringify(data);
          var obj2 = JSON.parse(str2);
          console.log(obj2.success);

          if(obj2.success == 1){
            console.log(data);
            this.storage.set('address_id', obj2.address_id);
            this.address_id = obj2.address_id;
            this.addressType = obj2.address_type;
            this.landmark = obj2.landmark;
            this.lat = obj2.latitude;
            this.lng = obj2.longitude;

            if(this.lat != null && this.lng != null)
            {
              let latLng = new google.maps.LatLng(this.lat, this.lng);
              let mapOptions = {
                center: latLng,
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              };

              this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

              var marker = new google.maps.Marker({
                map: this.map,
                position: latLng
              });

            }



          }else{
            console.log("No database record.")
            this.storage.get('address_id').then((val) => {
              this.address_id = val;
              console.log("Address id:"+this.address_id);
            });

            if(this.selectedAddress != null){
              console.log("Edited Address");
              console.log("Address:"+this.selectedAddress);
              this.storage.get('address_type').then((val) => {
                if(val != null){
                  this.addressType = val;
                }
              });
              this.storage.get('landmark').then((val) => {
                if(val != null){
                  this.landmark = val;
                }
              });

              this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address="+this.selectedAddress)
                .subscribe(data => {
                  console.log(data);

                  var str = JSON.stringify(data);
                  var obj = JSON.parse(str);
                  this.lat = obj.results[0].geometry.location.lat;
                  this.lng = obj.results[0].geometry.location.lng;

                  if(this.lat != null && this.lng != null)
                  {
                    let latLng = new google.maps.LatLng(this.lat, this.lng);
                    let mapOptions = {
                      center: latLng,
                      zoom: 18,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    }

                    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

                    var marker = new google.maps.Marker({
                      map: this.map,
                      position: latLng
                    });
                  }

                });

            }


          }





          /*this.DBaddress_id = obj2.address_id;
          this.storage.set('address_id', this.DBaddress_id);

          this.DBcustomer_id = obj2.customer_id;
          this.DBfirstname = obj2.firstname;
          this.DBlastname = obj2.lastname;
          this.DBmobile = obj2.mobile;
          this.DBadressType = obj2.address_type;
          this.DBaddress = obj2.address;
          this.DBlandmark = obj2.landmark;
          this.DBlatitude = obj2.latitude;
          this.DBlongitude = obj2.longitude;




          this.addressType = this.DBadressType;
          this.landmark = this.DBlandmark;
          this.lat = this.DBlatitude;
          this.lng = this.DBlongitude;*/


          /*if(obj2.success == null){

            console.log("No data");

            this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address="+this.selectedAddress)
              .subscribe(data => {
                console.log(data);

                var str = JSON.stringify(data)
                var obj = JSON.parse(str);
                this.lat = obj.results[0].geometry.location.lat;
                this.lng = obj.results[0].geometry.location.lng;

                let latLng = new google.maps.LatLng(this.lat, this.lng);

                let mapOptions = {
                  center: latLng,
                  zoom: 18,
                  mapTypeId: google.maps.MapTypeId.ROADMAP


                }

                this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

                var marker = new google.maps.Marker({
                  map: this.map,
                  position: latLng
                });

              })



          }else{
            console.log(obj2.success);
            // this.storage.set('address_id', obj2.address_id);
          }*/


          /*if((this.DBlatitude != null && this.DBlongitude != null) || this.selectedAddress != null){
            this.isMapEnabled = true;
            let latLng = new google.maps.LatLng(this.DBlatitude, this.DBlongitude);

            let mapOptions = {
              center: latLng,
              zoom: 18,
              mapTypeId: google.maps.MapTypeId.ROADMAP


            }

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            var marker = new google.maps.Marker({
              map: this.map,
              position: latLng
            });
          }*/



        })



    })

  }

  ngOnInit() {
    this.data.myCurrentAddress.subscribe(val => this.address = val);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressPage');
    setTimeout(() => {
      console.log("THIS DATA WILL BE SENT WHEN SAVE IS CLICKED.");
      console.log("customer_id: "+this.customer_id);
      console.log("firstname: "+this.firstname);
      console.log("lastname: "+this.lastname);
      console.log("mobile: "+this.mobile);
      console.log("address_id: "+this.address_id);
      console.log("address: "+this.selectedAddress);
      console.log("address_type: "+this.addressType);
      console.log("landmark: "+this.landmark);
      console.log("latitude: "+this.lat);
      console.log("longitude: "+this.lng);

    }, 3000);
  }


  loadMap(){

    let latLng = new google.maps.LatLng(14.59996688, 120.97449152);

    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP


    }



    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  }

  loadMyLoc(){

/*


    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address="+this.lat+","+this.lng)
      .subscribe(data => {
        console.log(data);
        var str = JSON.stringify(data)
        var obj = JSON.parse(str);
        console.log(obj.results[0]);
        console.log(obj.results[0].formatted_address);
        console.log(obj.results[0].geometry.location.lat);
        console.log(obj.results[0].geometry.location.lng);

        this.selectedAddress = obj.results[0].formatted_address;
        infowindow.setContent(obj.results[0].formatted_address);

      })
*/
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latLng,
      zoom: 20,
    });

    var marker = new google.maps.Marker({
      map: map,
      position: latLng
    });

  }


 test(){
    if(this.lat == null || this.lng == null){
      alert("Oops! something went wrong please try again.");
    }else{


    console.log("DATA SENT:");
    console.log("customer id:"+ this.customer_id);
    console.log("address id:"+ this.address_id);
    console.log("address:"+this.selectedAddress);
    console.log("address type:"+this.addressType);
    console.log("landmark:"+this.landmark);
    console.log("latitude:"+this.lat);
    console.log("longitude:"+this.lng);
    console.log("firstname:"+this.firstname);
    console.log("lastname:"+this.lastname);
    console.log("mobile:"+this.mobile);
    console.log("SAVED!");
    // alert("Address has been saved.");

    if(this.address_id == null || this.address_id == "")
    {
      this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_add_address.php?address_id=&customer_id="+this.customer_id+"&firstname="+this.firstname+"&lastname="+this.lastname+"&mobile="+this.mobile+"&address="+this.selectedAddress+"&address_type="+this.addressType+"&landmark="+this.landmark+"&latitude="+this.lat+"&longitude="+this.lng)
        .subscribe(data => {
          console.log(data);
          alert("Address has been saved.");
      })


    }

    let currentIndex = this.navCtrl.getActive().index;
    console.log("from address to myinfo:",currentIndex);

    this.navCtrl.push(MyinfoPage).then(() => {
    this.navCtrl.remove(currentIndex);
    });

    }

 }





  revgeocode(){

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 20,
      center: {lat: 414.59996688, lng: 120.97449152}
    });
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;

    this.geocodeLatLng(geocoder, map, infowindow);


  }


  geocodeLatLng(geocoder, map, infowindow) {
    var input = this.latLng;
    var latlngStr = input.split(',', 2);
    var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
    console.log("var latlng: ", latlng);
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          map.setZoom(11);
          var marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
          infowindow.setContent(results[0].formatted_address);
          console.log("info window: ", results[0].formatted_address);
          infowindow.open(map, marker);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }


  geocode(){
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 20,
      center: {lat: 414.59996688, lng: 120.97449152}
    });
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow;

    this.geocodeAddress(geocoder, map, infowindow);
    this.lat = this.holderLat;
    this.lng = this.holderLng;

  }


  geocodeAddress(geocoder, resultsMap, infowindow) {

    var address = this.selectedAddress;
    geocoder.geocode({'address': address}, function (results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);

        console.log("geometry: ", JSON.stringify(results[0].geometry.location));
        var obj = JSON.stringify(results[0].geometry.location);
        console.log("geometry object: ",JSON.parse(obj));
        var coordinates = JSON.parse(obj);
        console.log("coordinates object: ",coordinates);
        this.holderLat = coordinates.lat;
        console.log(this.holderLat);
        this.holderLng = coordinates.lng;
        console.log(this.holderLng );



        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });

        infowindow.setContent(results[0].formatted_address);
        infowindow.open(resultsMap, marker);

      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });


  }

  myLocation(){
    this.geo.getCurrentPosition().then( pos => {
      this.lat = pos.coords.latitude;
      this.lng = pos.coords.longitude;

    })
  }

  searchAddress()
  {

    let latLng = new google.maps.LatLng(14.59996688, 120.97449152);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latLng,
      zoom: 18,
    });

    var infowindow = new google.maps.InfoWindow;

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');

    var searchBox = new google.maps.places.SearchBox(<HTMLInputElement>input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
      var str = JSON.stringify(places);
      var obj = JSON.parse(str);
      console.log(str);
      console.log(obj[0].formatted_address);
      console.log(obj[0].geometry.location.lat);

      console.log(obj[0].geometry.location.lng);
      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          position: place.geometry.location,
          draggable:true
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

  }

  onOpenPlace(){
    this.modalCtrl.create(PlacePage).present();
  }
  goToSearch() {

    if(this.addressType != null || this.landmark != null){
      this.storage.set('address_type', this.addressType);
      this.storage.set('landmark', this.landmark);
    }

    let currentIndex = this.navCtrl.getActive().index;
    console.log("from address to search index:",currentIndex);

    this.navCtrl.push(SearchaddPage).then(() => {
      // this.navCtrl.remove(currentIndex);
    });


    /*let currentIndex = this.navCtrl.getActive().index;
    console.log(currentIndex);

    this.navCtrl.push(SearchaddPage);*/
  }


}
