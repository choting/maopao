import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage implements OnInit {

  showCheckbox: boolean = false;
  selectedCount = 0;

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  favoritesIsEmpty: boolean = true;

  constructor(public navCtrl: NavController, private data: DataserviceProvider) {}

  checkIfEmpty() {
    if (this.products.some(prod => prod.inFavorites == true)) {
      this.favoritesIsEmpty = false;
    } else {
      this.favoritesIsEmpty = true;
    }  
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
    this.checkIfEmpty();
  }

  remove() {
    this.products.forEach(function (product, pi) {
      if (product.toRemove) {
        // change css class to removing to appy transition effect
        product.removing = true;
        product.toRemove = false;
        // add deley to removing from favorites to wait for transition to take place
        setTimeout( () => { this.data.updateFavorites(pi, (product.inFavorites = false)); this.checkIfEmpty() }, 300);
      }
    }.bind(this));
    this.showCheckbox = false;
    this.selectedCount = 0;
  }

  cancelRemove() {
    this.products.forEach(function (product) {
      product.toRemove = false;
    });
    this.showCheckbox = false;
    this.selectedCount = 0;
  }

  updateCount(i, val) {
    if (val) {
      this.products[i].toRemove = false;
      --this.selectedCount;
    } else {
      this.products[i].toRemove = true;
      ++this.selectedCount;
    }
  }

  addToCart(id) {
    if (!this.showCheckbox) {
      let product = this.products.find(p => p.prodId == id);
      this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
      let store = this.stores.find(store => store.storeId == product.storeId);
      this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
      this.data.updateCartCount(++this.cartCount)
    }
  }

  removeFromCart(id) {
    if (!this.showCheckbox) {
      let product = this.products.find(p => p.prodId == id);
      this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
      let store = this.stores.find(store => store.storeId == product.storeId);
      this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
      if ((store.cartTotal == 0) && (store.inCheckout == true)) { 
        this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
        if (store.state) {
          this.data.updateCheckoutCount(--this.checkoutCount);
        } else {
          this.data.updateAdvancedCount(--this.advancedCount);
        } 
      }
      this.data.updateCartCount(--this.cartCount);
    }
  }

}
