import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the QaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-qa',
  templateUrl: 'qa.html',
})
export class QaPage implements OnInit {

  stores = [];
  store: any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.store = this.stores.find(s => s.storeId == this.navParams.get("storeId"));
  }

}
