import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { HomePage } from '../home/home';
import { MyinfoPage } from '../myinfo/myinfo';
import { OrdersPage } from '../orders/orders';
import { Http } from "@angular/http";
import { NetworkEngineProvider } from "../../providers/network-engine/network-engine";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Storage } from '@ionic/storage';
import {ProfilePage} from "../profile/profile";
import {AddressPage} from "../address/address";

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
  providers: [NetworkEngineProvider]

})
export class AuthPage implements OnInit {

  signUp: boolean = true;
  umobile: string = null;
  name: string = null;
  pword: string = null;
  userid: any;
  mobile: any;
  password: any;
  cPassword: any;
  confirmMessage: string = null;

  loggedIn: boolean = false;

  logged:any;
  id:any;

  from: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private http: HttpClient, public network: NetworkEngineProvider, private storage: Storage) {
    this.from = this.navParams.get('from');
  }

  ngOnInit() {
    this.data.currentLoggedIn.subscribe(val => this.loggedIn = val)
  }

  /*auth() {
    console.log(this.uname, this.pword);
    if (this.uname == "fwish" && this.pword == "2301") {
      console.log("true");
      this.data.updateLoggedInValue(this.loggedIn = true);
      if (this.from === "profile") {
        this.navCtrl.push(MyinfoPage)
      } else if (this.from === "cart") {
        this.navCtrl.push(OrdersPage)
      }
    }
  }*/

  ionViewDidLoad() {
    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidLoad AuthPage');
    console.log("==========================");
    this.storage.get('success').then((val) => {
      console.log('LOGIN:', val);
    });
    this.storage.get('customer_id').then((val) => {
      console.log('USER ID:', val);
    });

  }


  auth2(m, p) {

    console.log(this.umobile, this.pword);

    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_login_customer.php?umobile='+m+'&pword='+p, {})
        .subscribe(data => {
          console.log(data)
          console.log("result: "+data["success"])

          if(data["success"] == 1)
          {
            this.storage.set('success', data["success"]);
            this.storage.set('customer_id', data["customer_id"]);
            this.storage.set('firstname', data["firstname"]);
            this.storage.set('lastname', data["lastname"]);
            this.storage.set('mobile', data["mobile"]);
            this.storage.set('email', data["email"]);
            // this.data.updateLoggedInValue(this.loggedIn = true);

            if (this.from === "profile") {

              this.data.updateLoggedInValue(this.loggedIn = true);
              let currentIndex = this.navCtrl.getActive().index;
              console.log("current index", currentIndex);

              this.navCtrl.setRoot(ProfilePage);

            } else if (this.from === "cart") {
              this.data.updateLoggedInValue(this.loggedIn = true);
              this.navCtrl.setRoot(OrdersPage);

            }

          }

        })


  }


  RegisterAccount(mobileNumber, pass)
  {

    /*let p = this.network.callPost(mobileNumber, pass);
    p.then(data => {
      console.log("Received: " + JSON.stringify(data.json()));*/
    var Hotline = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
    var mobilePh = /^(\+?\d{2}?\s?\d{3}\s?\d{3}\s?\d{4})|([0]\d{3}\s?\d{3}\s?\d{4})$/;
    var password = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,40})$/;

    if(this.password == "" || this.cPassword == "" || this.password == null || this.cPassword == null)
    {
      console.log("You must fill in all of the fields.");
      this.confirmMessage = "You must fill in all of the fields.";
    }
    else if (!this.mobile.match(mobilePh))
    {
      this.confirmMessage = "Invalid Phone Number!";
      console.log("Invalid Phone Number!");
    }
    else if (!this.password.match(password))
    {
      console.log("Password must be number & character 8-40 & no special character.")
      this.confirmMessage = "Password must be number & character 8-40 & no special character.";
    }
    else if (this.password != this.cPassword){
      console.log("Password did not match!");
      this.confirmMessage = "Password did not match!";
      /*
      this.data.updateLoggedInValue(this.loggedIn = true);
      this.navCtrl.push(MyinfoPage);*/
    }
    else if (this.password == this.cPassword){
    console.log("Mobile Number:"+mobileNumber+" "+ "Password: "+pass);

    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_register_customer.php?mobile='+mobileNumber+'&password='+pass, {})
      .subscribe(data => {
        console.log(data)
        this.signUp = false;

      })

    }



  }


  /*testReq(mobile, password){

    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    let body = {
      mobile:mobile,
      password:password
    };

    console.log("body: "+JSON.stringify(body));

    console.log("Mobile Number:"+mobile+" "+ "Password: "+password);

    this.http.post('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_register_customer.php', JSON.stringify(body), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
      .subscribe(data => {
        console.log(data)
      })

  }*/



}
