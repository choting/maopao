import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CheckoutPage } from '../checkout/checkout';
import { OrderhistoryPage } from '../orderhistory/orderhistory';
import { AuthPage } from '../auth/auth';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})

export class OrdersPage implements OnInit {

  // state of the segment view for choosing cart or order history
  view: string = "myCart";

  user = {};

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;
  friends = [];

  friendSelected = null;

  orders = [];

  // state of store toggle for viewing open and closed stores
  storeState: boolean = true;
  storeStateLabel = "Open Stores";

  // modal states
  openModal: boolean = false;
  goToNextModal: boolean = false;

  //setting recepient for checkout
  recepient: Object = null;

  loggedIn: boolean = false;

  customer_id:any;
  firstname:any;
  lastname:any;
  fullname:any;
  mobile:any;
  email:any;
  friend_lists:any;

  customer_id_checkout:any;
  selectedRow:any;

  orders_table:any;
  arr_orders = [];

  constructor(public navCtrl: NavController, private data: DataserviceProvider, public params: NavParams, private storage: Storage, private http: HttpClient) {
    var from = params.get('from');
    if (from == "confirmPage") {
      this.view = "myOrderHistory";
    }



  }

  ionViewDidLoad() {

    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidLoad OrdersPage');
    console.log("==========================");

    // this.getAccountInfo();
  }

  ionViewDidEnter() {
    this.info();
    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidEnter OrdersPage');
    console.log("==========================");

    this.storage.get('customer_id').then((customer_id) => {
      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_order.php?customer_id=' + customer_id)
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          this.orders_table = obj;
          console.log(this.orders_table);


          if(this.orders_table.success != 3){
            console.log(this.orders_table.order);
            console.log(this.orders_table.order.length);

            this.arr_orders  = this.orders_table.order;
            console.log("ARRAY:", this.arr_orders);

            for(var a = 0; a < this.orders_table.order.length; a++){
              console.log("~");
              console.log(this.orders_table.order[a].ORDER_ID);
              console.log(this.orders_table.order[a].DELIVERY_DATE);
              console.log(this.orders_table.order[a].ORDER_STATUS);
              console.log(this.orders_table.order[a].first_store_logo);
            }
          }else{
            var null_array = [];
            this.arr_orders  = null_array;
          }




        });
    });
  }

  info(){
    this.storage.get('firstname').then((firstname) => {
      this.firstname = firstname;
      this.storage.get('lastname').then((lastname) => {
        this.lastname = lastname;
        this.storage.get('email').then((email) => {
          this.fullname = this.firstname+ " "+this.lastname;
          this.email = email;
          this.storage.get('mobile').then((mobile) => {
            this.mobile = mobile;
            this.storage.get('customer_id').then((customer_id) => {
              this.customer_id = customer_id;


              console.log("USER ID:",customer_id);
              console.log(this.fullname);
              console.log(this.firstname);
              console.log(this.lastname);
              console.log(this.email);
              console.log(this.mobile);

              this.myFriends();
            });
          });
        });
      });
    });
  }

  myFriends(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.customer_id)
      .subscribe(fl => {
        var str = JSON.stringify(fl);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("NO RESULT!");
          console.log(obj);
        }else{
          console.log("FRIENDS:",obj);
          this.friend_lists = obj.friend_request;
          // this.friend_lists_count = obj.friend_request.length;
          // this.storage.set('my_friend_lists', obj);

        }

      });
  }

  setClickedRow(index, f_id){
    this.customer_id_checkout = f_id;
    this.selectedRow = index;
    console.log("clicked!", this.customer_id_checkout);
  }

  ngOnInit() {
    this.data.currentLoggedIn.subscribe(val => this.loggedIn = val);
    this.data.currentStoreState.subscribe(val => this.storeState = val);
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
    this.data.currentFriends.subscribe(val => this.friends = val);
    this.data.currentFriendSelected.subscribe(val => this.friendSelected = val);
    this.data.currentOrders.subscribe(val => this.orders = val);
    this.data.currentUser.subscribe(val => this.user = val);

    this.orders.forEach(function(order, index) {
      var store = this.stores.find(s => s.storeId == order.storeId);

      if (order.deliveryState < 4) {

        var row = this.friends.find(row => row.some(friend => friend.id == order.recepientId));
        var f = row.find(f => f.id == order.recepientId);
        order.recepientName = f.name;
        order.logo = f.image;

      } else if (order.deliveryState > 3 && order.deliveryState < 8) {

        var row = this.friends.find(row => row.some(friend => friend.id == order.senderId));
        var f = row.find(f => f.id == order.senderId);
        order.senderName = f.name;
        order.logo = f.image;

      } else if (order.deliveryState > 7) {
        order.logo = store.logo;
      }

    }.bind(this));
  }




  // toggling segment
  segmentChanged($event) {
    this.view;
  }




  // toggling stores
  storeStateToggled($event) {
    console.log(this.storeState)
    if (this.storeState) {
      this.storeStateLabel = "Open Stores"
    } else {
      this.storeStateLabel = "Closed Stores"
    }
    this.data.updateStoreState(this.storeState);
  }




  //toggling stores for checkout
  updateCheckout(val) {
    if (val) {
      this.data.updateCheckoutCount(++this.checkoutCount);
    } else {
      this.data.updateCheckoutCount(--this.checkoutCount);
    }
  }




  //toggling stores for checkout
  updateAdvanced(val) {
    if (val) {
      this.data.updateAdvancedCount(++this.advancedCount)
    } else {
      this.data.updateAdvancedCount(--this.advancedCount)
    }
  }




  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }




  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) {
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      }
    }
    this.data.updateCartCount(--this.cartCount);
  }




  setRecepient(id) {

    let row = this.friends.find(row => row.some(friend => friend.id == id));
    let f = row.find(f => f.id == id);
    this.data.updateFriends(id, (f.set = true));

    if (this.friendSelected !== null) {
      let prevRow = this.friends.find(row => row.some(friend => friend.id == this.friendSelected));
      let prevF = prevRow.find(f => f.id == this.friendSelected);
      this.data.updateFriends(prevF.id, (prevF.set = false));
    }

    this.data.setCurrentFriendSelected(this.friendSelected = id);

  }




  removeRecepient(id) {

    var row = this.friends.find(row => row.some(friend => friend.id == id));
    var f = row.find(f => f.id == id);

    this.data.updateFriends(id, (f.set = false));
    this.data.setCurrentFriendSelected(this.friendSelected = null);

  }




  checkoutForSelf(id) {

    // unset friends if any selected
    /*if (this.friendSelected !== null) {
      this.removeRecepient(this.friendSelected);
      this.data.setCurrentFriendSelected(this.friendSelected = null);
    }*/

    if(id != null){
      this.goToCheckout(id);
      this.storage.set("check_out_friend_name", null);
      this.storage.set("check_out_friend", null);
      console.log("recipient:",this.customer_id);
      this.storage.set("recipient_id", this.customer_id);
    }else{
      console.log("error no id");
    }

  }

  checkoutForFriend(friend_id) {


    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_getinfo.php?id="+friend_id).subscribe(info => {
      var str = JSON.stringify(info);
      var obj = JSON.parse(str);

      console.log("ID:", friend_id);
      var fullname = obj.firstname+" "+obj.lastname;
      console.log("FRIEND RECIPIENT:", fullname);
      this.storage.set("check_out_friend", 1);
      this.storage.set("check_out_friend_name", fullname);

      console.log("recipient:",friend_id);
      this.storage.set("recipient_id", friend_id);
      this.goToCheckout(friend_id);

    })

    // this.storage.set("check_out_friend", 1);
    // this.goToCheckout(friend_id);
  }




  goToCheckout(id) {
    var str_id = id;

    if(id != null){
      this.storage.set("checkout_customer_id", str_id);
      console.log("CHECKOUT CUSTOMER ID:",id);


      var s = [];
      var p = [];

      this.stores.forEach((store) => {
        if (this.storeState && store.inCheckout && store.state) {
          s.push(store)
          this.products.forEach((product) => {
            if (product.quantity > 0 && product.storeId == store.storeId) {
              p.push(product)
            }
          });
        } else if (!this.storeState && store.inCheckout && !store.state) {
          s.push(store)
          this.products.forEach((product) => {
            if (product.quantity > 0 && product.storeId == store.storeId) {
              p.push(product)
            }
          });
        }
      });

      // go to checkout
      this.navCtrl.push(CheckoutPage, {
        storeState: this.storeState,
        stores: s,
        products: p
      });

      this.openModal = false;
      this.goToNextModal = false;

    }else{
      console.log("NO ID FAILED!");
    }


  }



  goToOrderHistory(id) {
    /*this.navCtrl.push(OrderhistoryPage, {
      orderId: id
    });*/

    /*console.log("order history clicked id:",id);
    this.storage.set('order_history_id', id);
    this.navCtrl.push(OrderhistoryPage);*/
    console.log(id);

    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order.php?order_id='+id)
      .subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        console.log(obj);

        console.log("orderid:",obj.order[0].ORDER_ID);
        this.storage.set('order.ORDER_ID', obj.order[0].ORDER_ID);

        console.log("from:",obj.order[0].CUSTOMER_ID);
        this.storage.set('order.CUSTOMER_ID', obj.order[0].CUSTOMER_ID);

        console.log("to:",obj.order[0].RECIPIENT_ID);
        this.storage.set('order.RECIPIENT_ID', obj.order[0].RECIPIENT_ID);

        console.log("status:",obj.order[0].ORDER_STATUS);
        this.storage.set('order.ORDER_STATUS', obj.order[0].ORDER_STATUS);

        console.log("gift:",obj.order[0].gift);
        this.storage.set('order.gift', obj.order[0].gift);

        console.log("giftstatus:",obj.order[0].gift_status);
        this.storage.set('order.gift_status', obj.order[0].gift_status);

        console.log("gift_fname:",obj.order[0].firstname);
        this.storage.set('order.firstname', obj.order[0].firstname);

        console.log("gift_lname:",obj.order[0].lastname);
        this.storage.set('order.lastname', obj.order[0].lastname);

        this.navCtrl.push(OrderhistoryPage);

      });
  }

  checkAuth() {
    this.storage.get("success").then( log => {
      if(log == 1) {

        this.openModal = !this.openModal;

      }
      else
      {

        this.navCtrl.setRoot(AuthPage, {
          from: "cart"
        });
      }
      });

    /*if (this.loggedIn) {
      this.openModal = !this.openModal
    } else {
      this.navCtrl.push(AuthPage, {
        from: "cart"
      })
    }*/

  }

}
