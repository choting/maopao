import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DragulaService, dragula } from 'ng2-dragula';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the VendingmachinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-vendingmachine',
  templateUrl: 'vendingmachine.html',
})
export class VendingmachinePage implements OnDestroy, OnInit {

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private dragulaService: DragulaService, private data: DataserviceProvider) {
  
    dragulaService.setOptions('bag-one', {
      copy: function(el,source) {
        return source.classList.contains('vending-category')
      },
      revertOnSpill: true,
      accepts: function(el, target, source, sibling) {
        return target.classList.contains('drop-area')
      },
      removeOnSpill: function(el, source) {
        return source.classList.contains('drop-area')
      },
      copySortSource: false
    })

    dragulaService.dropModel.subscribe((value) => {
      let [bag, el, target, source] = value;
      
      if (bag == "bag-one" && source.classList.contains('vending-category')) {
        console.log(el.getAttribute('data-id'), "from vending")
        this.addToCart(el.getAttribute('data-id'))
      }
    })

    dragulaService.remove.subscribe((value) => {
      let [e, el, container, source] = value;
      this.removeFromCart(el.getAttribute('data-id'))
    })

  }

  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }

  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) { 
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      } 
    }
    this.data.updateCartCount(--this.cartCount);
  }

  ngOnDestroy() {
    this.dragulaService.destroy('bag-one');
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
  }

  

  category_1 = [
    { prodId: 15,   vending_image: "assets/imgs/vending_machine/d1.png" },
    { prodId: 16,   vending_image: "assets/imgs/vending_machine/d2.png" },
    { prodId: 17,   vending_image: "assets/imgs/vending_machine/d3.png" },
    { prodId: 18,   vending_image: "assets/imgs/vending_machine/d4.png" },
    { prodId: 19,   vending_image: "assets/imgs/vending_machine/d5.png" },
    { prodId: 20,   vending_image: "assets/imgs/vending_machine/d6.png" },
    { prodId: 21,   vending_image: "assets/imgs/vending_machine/d7.png" },
    { prodId: 22,   vending_image: "assets/imgs/vending_machine/d8.png" },
    { prodId: 23,   vending_image: "assets/imgs/vending_machine/d9.png" },
    { prodId: 24,   vending_image: "assets/imgs/vending_machine/d10.png" },
    { prodId: 25,   vending_image: "assets/imgs/vending_machine/d11.png" },
    { prodId: 26,   vending_image: "assets/imgs/vending_machine/d12.png" },
    { prodId: 27,   vending_image: "assets/imgs/vending_machine/d13.png" },
    { prodId: 28,   vending_image: "assets/imgs/vending_machine/d14.png" }
  ]

  category_2 = [
    { prodId: 15,   vending_image: "assets/imgs/vending_machine/d1.png" },
    { prodId: 16,   vending_image: "assets/imgs/vending_machine/d2.png" },
    { prodId: 17,   vending_image: "assets/imgs/vending_machine/d3.png" },
    { prodId: 18,   vending_image: "assets/imgs/vending_machine/d4.png" },
    { prodId: 19,   vending_image: "assets/imgs/vending_machine/d5.png" },
    { prodId: 20,   vending_image: "assets/imgs/vending_machine/d6.png" },
    { prodId: 21,   vending_image: "assets/imgs/vending_machine/d7.png" },
    { prodId: 22,   vending_image: "assets/imgs/vending_machine/d8.png" },
    { prodId: 23,   vending_image: "assets/imgs/vending_machine/d9.png" },
    { prodId: 24,   vending_image: "assets/imgs/vending_machine/d10.png" },
    { prodId: 25,   vending_image: "assets/imgs/vending_machine/d11.png" },
    { prodId: 26,   vending_image: "assets/imgs/vending_machine/d12.png" },
    { prodId: 27,   vending_image: "assets/imgs/vending_machine/d13.png" },
    { prodId: 28,   vending_image: "assets/imgs/vending_machine/d14.png" }
  ]

  category_3 = [
    { prodId: 15,   vending_image: "assets/imgs/vending_machine/d1.png" },
    { prodId: 16,   vending_image: "assets/imgs/vending_machine/d2.png" },
    { prodId: 17,   vending_image: "assets/imgs/vending_machine/d3.png" },
    { prodId: 18,   vending_image: "assets/imgs/vending_machine/d4.png" },
    { prodId: 19,   vending_image: "assets/imgs/vending_machine/d5.png" },
    { prodId: 20,   vending_image: "assets/imgs/vending_machine/d6.png" },
    { prodId: 21,   vending_image: "assets/imgs/vending_machine/d7.png" },
    { prodId: 22,   vending_image: "assets/imgs/vending_machine/d8.png" },
    { prodId: 23,   vending_image: "assets/imgs/vending_machine/d9.png" },
    { prodId: 24,   vending_image: "assets/imgs/vending_machine/d10.png" },
    { prodId: 25,   vending_image: "assets/imgs/vending_machine/d11.png" },
    { prodId: 26,   vending_image: "assets/imgs/vending_machine/d12.png" },
    { prodId: 27,   vending_image: "assets/imgs/vending_machine/d13.png" },
    { prodId: 28,   vending_image: "assets/imgs/vending_machine/d14.png" }
  ]

  category_4 = [
    { prodId: 15,   vending_image: "assets/imgs/vending_machine/d1.png" },
    { prodId: 16,   vending_image: "assets/imgs/vending_machine/d2.png" },
    { prodId: 17,   vending_image: "assets/imgs/vending_machine/d3.png" },
    { prodId: 18,   vending_image: "assets/imgs/vending_machine/d4.png" },
    { prodId: 19,   vending_image: "assets/imgs/vending_machine/d5.png" },
    { prodId: 20,   vending_image: "assets/imgs/vending_machine/d6.png" },
    { prodId: 21,   vending_image: "assets/imgs/vending_machine/d7.png" },
    { prodId: 22,   vending_image: "assets/imgs/vending_machine/d8.png" },
    { prodId: 23,   vending_image: "assets/imgs/vending_machine/d9.png" },
    { prodId: 24,   vending_image: "assets/imgs/vending_machine/d10.png" },
    { prodId: 25,   vending_image: "assets/imgs/vending_machine/d11.png" },
    { prodId: 26,   vending_image: "assets/imgs/vending_machine/d12.png" },
    { prodId: 27,   vending_image: "assets/imgs/vending_machine/d13.png" },
    { prodId: 28,   vending_image: "assets/imgs/vending_machine/d14.png" }
  ]

  category_5 = [
    { prodId: 15,   vending_image: "assets/imgs/vending_machine/d1.png" },
    { prodId: 16,   vending_image: "assets/imgs/vending_machine/d2.png" },
    { prodId: 17,   vending_image: "assets/imgs/vending_machine/d3.png" },
    { prodId: 18,   vending_image: "assets/imgs/vending_machine/d4.png" },
    { prodId: 19,   vending_image: "assets/imgs/vending_machine/d5.png" },
    { prodId: 20,   vending_image: "assets/imgs/vending_machine/d6.png" },
    { prodId: 21,   vending_image: "assets/imgs/vending_machine/d7.png" },
    { prodId: 22,   vending_image: "assets/imgs/vending_machine/d8.png" },
    { prodId: 23,   vending_image: "assets/imgs/vending_machine/d9.png" },
    { prodId: 24,   vending_image: "assets/imgs/vending_machine/d10.png" },
    { prodId: 25,   vending_image: "assets/imgs/vending_machine/d11.png" },
    { prodId: 26,   vending_image: "assets/imgs/vending_machine/d12.png" },
    { prodId: 27,   vending_image: "assets/imgs/vending_machine/d13.png" },
    { prodId: 28,   vending_image: "assets/imgs/vending_machine/d14.png" }
  ]

  dropArea = [

  ]

}
