import { Component, OnInit } from '@angular/core';

import { HomePage } from '../home/home';
import { FavoritesPage } from '../favorites/favorites';
import { OrdersPage } from '../orders/orders';
import { ProfilePage } from '../profile/profile';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {NavController} from "ionic-angular";

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FavoritesPage;
  tab3Root = OrdersPage;
  tab4Root = ProfilePage;

  cartCount = 0;

  constructor(private data: DataserviceProvider) {}


  ngOnInit() {
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
  }

  homeIcon = "custom-home";
  favoritesIcon = "favorites";
  menuIcon = "custom-menu";
  ordersIcon = "orders";
  profileIcon = "profile";

  toggleSelect(icon) {

    if(icon == this.homeIcon) {
      if (this.homeIcon == "custom-home") {
        this.homeIcon = "custom-home-selected";
        this.favoritesIcon = "favorites";
        this.ordersIcon = "orders";
        this.profileIcon = "profile";
        this.menuIcon = "custom-menu";
      } else {
        this.homeIcon = "custom-home";
      }
    } else if (icon == this.favoritesIcon) {
      if (this.favoritesIcon == "favorites") {
        this.favoritesIcon = "favorites-selected";
        this.homeIcon = "custom-home";
        this.ordersIcon = "orders";
        this.profileIcon = "profile";
        this.menuIcon = "custom-menu";
      } else {
        this.favoritesIcon = "favorites";
      }
    } else if (icon == this.ordersIcon) {
      if (this.ordersIcon == "orders") {
        this.favoritesIcon = "favorites";
        this.homeIcon = "custom-home";
        this.ordersIcon = "orders-selected";
        this.profileIcon = "profile";
        this.menuIcon = "custom-menu";
      } else {
        this.favoritesIcon = "orders";
      }
    } else if (icon == this.profileIcon) {
      if (this.profileIcon == "profile") {
        this.favoritesIcon = "favorites";
        this.homeIcon = "custom-home";
        this.ordersIcon = "orders";
        this.profileIcon = "profile-selected";
        this.menuIcon = "custom-menu";
      } else {
        this.favoritesIcon = "profile";
      }
    }
  }



}
