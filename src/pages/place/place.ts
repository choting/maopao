import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import { Geolocation } from '@ionic-native/geolocation';
import {HttpClient} from "@angular/common/http";
import { HTTP } from '@ionic-native/http';
import {AddressPage} from "../address/address";
import {DataserviceProvider} from "../../providers/dataservice/dataservice";

@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})
export class PlacePage implements OnInit{

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker: any;

  address:any;

  deliveryAddress = null;
  isConfirmed: boolean = true;

  latitude: any;
  longitude: any;
  addressName: any;
  searchQuery: any;
  input: string = null;
  self = this;

  searchMsg: string = null;
  isMapEnabled:boolean = false;
  isListenerOn:boolean = false;

  searches:any;

  loggedIn: boolean = false;
  test = [];
  warning:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private viewCtrl: ViewController, private storage: Storage, private geo: Geolocation, private http: HttpClient, private req:HTTP) {
  }

  ionViewDidLoad() {
    this.geo.getCurrentPosition().then(pos => {
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
    })
    console.log('ionViewDidLoad PlacePage');

    // this.loadMap();


  }



  onDismiss() {
    this.viewCtrl.dismiss();
  }



  onConfirm() {
    this.isConfirmed = true;
    google.maps.event.clearListeners(this.map, 'dragend');

    var curMap = this.map;

    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lat()));
    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lng()));


    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address=" + curMap.getCenter().lat()+","+curMap.getCenter().lng())
      .subscribe(data => {

        var result = JSON.parse(JSON.stringify(data));

        // this.latitude = this.marker.getPosition().lat();
        // this.longitude = this.marker.getPosition().lng();
        var str = JSON.stringify(data)
        var obj = JSON.parse(str);
        this.addressName = result.results[0].formatted_address;

        console.log(this.addressName);

        var latStr = JSON.stringify(curMap.getCenter().lat());
        var lngStr = JSON.stringify(curMap.getCenter().lng());

        var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};

        this.marker = new google.maps.Marker({
          map: this.map,
          draggable: false,
          position: latlng
        });


        // this.storage.set('setAddress', this.addressName);

        // console.log(this.addressName);
        // console.log(this.latitude);
        // console.log(this.longitude);

      })



  }

  getCurrentLocation() {
    this.geo.getCurrentPosition().then(pos => {
      this.storage.set("myLat", pos.coords.latitude);
      this.storage.set("myLng", pos.coords.longitude);
    });
  }

  onLocate() {

    this.getCurrentLocation();

    this.isMapEnabled = true;
    this.isConfirmed = true;
    // google.maps.event.clearListeners(this.map, 'dragend');


    this.storage.get('myLat').then((val) => {
      console.log('Your lat is', val);
      this.latitude = val;

      this.storage.get('myLng').then((val) => {
        console.log('Your lng is', val);
        this.longitude = val;

        let latLng = new google.maps.LatLng(this.latitude, this.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng
        });

        var geocoder = new google.maps.Geocoder;

        var mapLoc = this.map;
        var inMarker = this.marker

        var latStr = JSON.stringify(this.latitude);
        console.log(latStr);
        var lngStr = JSON.stringify(this.longitude);
        console.log(lngStr);
        var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};

        geocoder.geocode({'location': latlng}, function(results) {
          if (results[0]) {

            var iWC = '<div id="mydiv">'+results[0].formatted_address+'</div>';

            var infowindow = new google.maps.InfoWindow({
              content: iWC,
            });
            console.log(results[0].formatted_address);
            infowindow.open(mapLoc, inMarker);
          } else {
            console.log('No results found');
          }
        });

      });

    });

    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address="+this.latitude+","+this.longitude)
      .subscribe(data => {

        var str = JSON.stringify(data)
        var obj = JSON.parse(str);
        this.addressName = obj.results[0].formatted_address;
        console.log(this.addressName);

      })



  }

  public run = () => { // <-- note syntax here
    // alert(this.latitude+","+this.longitude);
    var curMap = this.map;

    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lat()));
    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lng()));

    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address=" + curMap.getCenter().lat()+","+curMap.getCenter().lng())
      .subscribe(data => {
        var result = JSON.parse(JSON.stringify(data));

        alert(result.results[0].formatted_address);


      })
  }

  public displayPred = (predictions, status) => { // <-- note syntax here

    this.searches = predictions;
    console.log(this.searches);
    // console.log(JSON.stringify(predictions[0].description));
    // console.log(predictions[1].description);

    if (status != google.maps.places.PlacesServiceStatus.OK) {
      // alert(status);
      return;
    }

    predictions.forEach(function (prediction) {
      // console.log(prediction.description);

      // var li = document.createElement('li');
      // li.appendChild(document.createTextNode(prediction.description));
      // document.getElementById('results').appendChild(li);
    });

  }

  onSelectArea() {
    this.isMapEnabled = true;
    this.isConfirmed = false;


    this.storage.get('myLat').then((val) => {
      console.log('Your lat is', val);
      this.latitude = val;
      this.storage.get('myLng').then((val) => {
        console.log('Your lng is', val);
        this.longitude = val;

        let latLng = new google.maps.LatLng(this.latitude, this.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        // this.map.addListener('dragend', this.run);
        google.maps.event.addListener(this.map, 'dragend', this.run);


        /*this.marker = new google.maps.Marker({
          map: this.map,
          draggable: false,
          animation: google.maps.Animation.DROP,
          position: latLng
        });


        google.maps.event.addListener(this.map, 'drag', function() {
          console.log('Dragging...');
          // console.log(this.latLng);
          // updateMarkerPosition(marker.getPosition());
        });
        google.maps.event.addListener(this.map, 'dragend', this.getCenterAdd);

        /!*this.map.addEventListener(GoogleMapsEvent.MY_LOCATION_BUTTON_CLICK).subscribe(() => {
         console.log('Draging..');
        });*!/

        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow();

        var inMarker = this.marker;

        inMarker.addListener('click', toggleBounce);

        function toggleBounce() {
          if (inMarker.getAnimation() !== null) {
            inMarker.setAnimation(null);
            console.log(JSON.stringify(inMarker.getPosition()));
            infowindow.close();

          } else {
            inMarker.setAnimation(google.maps.Animation.BOUNCE);


            var latStr = JSON.stringify(inMarker.getPosition().lat());
            var lngStr = JSON.stringify(inMarker.getPosition().lng());

            var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};


            geocoder.geocode({'location': latlng}, function (results) {
              if (results[0]) {

                var iWC = infowindow.getContent();

                iWC = '<div id="mydiv">' + results[0].formatted_address + '</div>';

                infowindow.setContent(iWC);
                infowindow.open(this.map, inMarker);
              } else {
                window.alert('No results found');
              }
            });


            console.log(JSON.stringify(inMarker.getPosition()));
          }
        }*/


      });
    });
    //end of first





  }


  getCenterAdd(){
    console.log(this.latitude);
  }


  loadMap() {
    let latLng = new google.maps.LatLng(14.59996688, 120.97449152);

    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }


    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  }


  /*test(){
    window.addEventListener("keyup", log);
    window.addEventListener("keypress", log2);
    window.addEventListener("keydown", log3);
    function log(event){

      console.log( event.type );
    }
    function log2(event){
      console.log( event.type );
    }
    function log3(event){
      console.log( event.type );
    }
  }*/

  buttonTest(val){
    console.log(val);
    this.navCtrl.push(AddressPage, {
      address: val
    });
  }


  updateList(ev) {
    this.isMapEnabled = false;
    this.input = ev.target.value;
    if(this.input == "" || this.input == null){
      this.input = "asdfasdf";
      console.log("No result:"+this.input);
    }
    console.log(this.input)

    var service = new google.maps.places.AutocompleteService();
    var request = {
      input: this.input,
      componentRestrictions: {country: 'ph'},
    };

    service.getPlacePredictions(request, this.displayPred);






    /*this.http.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=sm%20bac&sensor=false&key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&components=country:ph"+this.input, {})
      .subscribe(data => {
      })*/



    //TEST AREA HERE


    /*var displaySuggestions = function (predictions, status) {

      // console.log(JSON.stringify(predictions[0].description));
      // console.log(predictions[1].description);

      if (status != google.maps.places.PlacesServiceStatus.OK) {
        alert(status);
        return;
      }

      predictions.forEach(function (prediction) {
        // console.log(prediction.description);

        // var li = document.createElement('li');
        // li.appendChild(document.createTextNode(prediction.description));
        // document.getElementById('results').appendChild(li);
      });

    };
    var service = new google.maps.places.AutocompleteService();
    var request = {
      input: ev.target.value,
      componentRestrictions: {country: 'ph'},
    };

    service.getPlacePredictions(request, displaySuggestions);*/

    //TEST AREA HERE






    /*this.http.get("https://maps.googleapis.com/maps/api/place/queryautocomplete/json?key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&input="+this.input)
      .subscribe(data => {

        /!*var str = JSON.stringify(data)
        var obj = JSON.parse(str);*!/

        var myStr = JSON.stringify(data["predictions"]);

        var myObject = JSON.parse(myStr);
        this.searches = myObject;
        console.log("LENGTH:"+myObject.length);


        for (var a = 0; a<myObject.length; a++) {

          //easy way to display object
          console.log(myObject[a].description);


          //complicated but same display to object
          var obj = JSON.parse(myStr);
          var arrName = JSON.stringify(obj[a]["description"]);
          console.log(arrName);
          this.searches.push(arrName);
        }

    // test
    console.log(obj.results[0].geometry.location.lat);
    var lat = obj.results[0].geometry.location.lat;
    console.log(obj.results[0].geometry.location.lng);
    var lng = obj.results[0].geometry.location.lng
    var coordinates = lat+","+lng;
    console.log(coordinates);
    // test for address search geocode api


    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address="+coordinates)
      .subscribe(data => {
        console.log(data);

        //
        var myStr = JSON.stringify(data["results"]);

        var myObject = JSON.parse(myStr);
        this.searches = myObject;
        console.log(myObject.length);


        for (var a = 0; a<5; a++){

          var obj = JSON.parse(myStr);
          var arrName = JSON.stringify(obj[a]["formatted_address"]);
          console.log(arrName);

        }

    setTimeout(() => {

        }, 2500);

      })


    })*/



  }

  searchList(val){
    console.log(val);
    this.searchQuery = "";
    this.searches = null;
  }


  save(){
    this.storage.set('placeAddress', this.searchQuery);
    this.navCtrl.push(AddressPage, {
      address: this.searchQuery
    });
  }



  ngOnInit() {
    this.data.currentLoggedIn.subscribe(val => this.loggedIn = val);
    this.data.myCurrentAddress.subscribe(val => this.address = val);
    this.deliveryAddress = this.address[0].value;
  }

  addAddress() {
    this.navCtrl.push(AddressPage);
  }

  goToAddress() {
    this.viewCtrl.dismiss();
  }
}
