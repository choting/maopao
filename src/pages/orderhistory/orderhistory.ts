import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the OrderhistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-orderhistory',
  templateUrl: 'orderhistory.html',
})
export class OrderhistoryPage implements OnInit {

  title:string = null;

  orders = [];
  stores = [];
  products = [];
  friends = [];
  myAddress = [];
  theirAddress = [];

  order:any = null;

  orderId:number = null;
  deliveryState:number = null;
  orderDate:string = null;
  storeDistance:number = null;
  eta:string = null;
  storeName:string = null;
  storeState: boolean = null;
  deliveryFee:number = null;
  storeTotal:number = null;
  address:string = null;
  deliveryMode:string = null;
  paymentMode:string = null;
  cardNumber:string = null;

  friendImage:string = 'assets/imgs/personicon.png';
  friendName:string = null;

  orderItems = [];

  inProcess: boolean = false;
  inTransit: boolean = false;
  delivered: boolean = false;


  delivery_date:any;
  gift = null;
  gift_status = null;

  store_req:any;
  product_req:any;
  order_req:any;
  order_status_req:any;

  store_address:any;
  delivery_distance:any;
  estimated_arrival_time:any;

  from:any;
  to:any;

  display_distance:any;

  mygift = null;
  yourgift = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider,private storage: Storage, private http: HttpClient, private req: HTTP) {

  }

  ngOnInit() {
    /*this.data.currentOrders.subscribe(val => this.orders = val);
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentFriends.subscribe(val => this.friends = val);
    this.data.myCurrentAddress.subscribe(val => this.myAddress = val);
    this.data.theirCurrentAddress.subscribe(val => this.theirAddress = val);

    let order = this.orders.find(o => o.orderId == this.navParams.get("orderId"));
    let store = this.stores.find(s => s.storeId == order.storeId);

    this.orderItems = order.products;
    this.orderId = order.orderId;
    this.deliveryState = order.deliveryState;
    this.deliveryMode = order.deliveryOptions;
    this.paymentMode = order.paymentOptions;
    this.orderDate = order.date;
    this.storeDistance = store.distance;
    this.eta = order.eta;
    this.storeName = store.name;
    this.storeState = store.state;
    this.storeTotal = this.deliveryFee = store.deliveryFee;

    if (order.deliveryState < 4 ) {

      let r = this.friends.find(row => row.some(friend => friend.id == order.recepientId));
      let f = r.find(f => f.id == order.recepientId);
      let a = this.theirAddress.find(a => a.id == f.id);
      this.friendName = f.name;
      this.friendImage = f.image;
      this.address = a.value;
      this.cardNumber = order.cardNumber;

    } else if (order.deliveryState > 3 && order.deliveryState < 8) {

      let r = this.friends.find(row => row.some(friend => friend.id == order.senderId));
      let f = r.find(f => f.id == order.senderId);
      let a = this.theirAddress.find(a => a.id == f.id);
      this.friendName = f.name;
      this.friendImage = f.image;
      this.address = a.value;

    } else if (order.deliveryState > 7) {

      let a = this.myAddress.find(a => a.id == 18);
      this.address = a.value;
      this.cardNumber = order.cardNumber;
      this.title = "Your Order";

    }

    if (order.deliveryState == 1 || order.deliveryState == 5 || order.deliveryState == 8) {
      this.inProcess = true;
    } else if (order.deliveryState == 2 || order.deliveryState == 6 || order.deliveryState == 9) {
      this.inProcess = true;
      this.inTransit = true;
    } else if (order.deliveryState == 3 || order.deliveryState == 7 || order.deliveryState == 10) {
      this.inProcess = true;
      this.inTransit = true;
      this.delivered = true;
    }

    this.orderItems.forEach(function(prod){
      let p = this.products.find(p => p.prodId == prod.prodId);
      prod.name = p.name;
      prod.image = p.image;
      prod.price = p.price;
      prod.quantityPrice = prod.price * prod.quantity;
      this.storeTotal += prod.quantityPrice;
    }.bind(this));

    https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order_product.php?order_id=201808293
    https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order.php?order_id=201808281
    'order.CUSTOMER_ID'
    'order.ORDER_ID'
    'order.RECIPIENT_ID'
    'order.ORDER_STATUS'
    'order.gift'
    'order.gift_status'
    'order.firstname'
    'order.lastname'
    */
  }

  ionViewDidEnter() {
    this.storage.get('customer_id').then( myid => {
      this.storage.get('order.ORDER_ID').then( orderid => {
        this.storage.get('order.CUSTOMER_ID').then( customer_id => {
        this.storage.get('order.RECIPIENT_ID').then( recipientid => {
          this.storage.get('order.ORDER_STATUS').then( orderstatus => {
            this.storage.get('order.gift').then( gift => {
              this.storage.get('order.gift_status').then( gift_status => {
              this.storage.get('order.firstname').then( firstname => {
                this.storage.get('order.lastname').then( lastname => {


                  if (recipientid == myid && gift == 1) {
                    this.friendName = firstname + " " + lastname;
                    this.mygift = 1;
                  } else if (myid == customer_id && gift == 1) {
                    this.yourgift = 1;
                  }else{
                  }

                  console.log('my id:', myid);
                  console.log('order_id:', orderid);
                  console.log('recipient_id:', recipientid);
                  console.log('order_status:', orderstatus);
                  console.log('gift:', gift);
                  console.log('gift_status:', gift_status);
                  console.log('firstname:', firstname);
                  console.log('lastname:', lastname);


                  });
                 });
                });
              });
            });
          });
        });
      });
    });

    this.storage.get('order.ORDER_ID').then( val => {


    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidEnter OrderHistory ID:', val);
    console.log("==========================");

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order_store.php?order_id='+val)
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          console.log(obj);
          this.store_req = obj.store;

          this.store_address = obj.store[0].address;
          console.log("store address:",this.store_address);


      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order_product.php?order_id='+val)
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          console.log(obj);
          this.product_req = obj.product;
        });

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order.php?order_id='+val)
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          console.log(obj);

          if(obj.order[0].gift == 1 && obj.order[0].gift_status == 0){
            this.gift = 1;
            this.gift_status = 0;
            this.title = "Your Gift";
            this.storeState = true;
            this.inProcess = true;

            this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_search_customer_id.php?customer_id='+obj.order[0].RECIPIENT_ID)
              .subscribe(data => {
                var str = JSON.stringify(data);
                var obj = JSON.parse(str);
                console.log(obj);
                this.friendName = obj.firstname + " " + obj.lastname;
              });
          }else if(obj.order[0].gift == 1 && obj.order[0].gift_status == 1){
            this.gift = 1;
            this.gift_status = 1;
            this.title = "Your Gift";
            this.storeState = true;
            this.inProcess = true;

            this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_search_customer_id.php?customer_id='+obj.order[0].RECIPIENT_ID)
              .subscribe(data => {
                var str = JSON.stringify(data);
                var obj = JSON.parse(str);
                console.log(obj);
                this.friendName = obj.firstname + " " + obj.lastname;
              });
          }

          else {
            this.gift = 0;
            this.title = "Your Order";
            this.storeState = true;
            this.inProcess = true;
          }


          this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+obj.order[0].DELIVERY_ADDRESS+'&key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0')
            .subscribe(data => {
              var str = JSON.stringify(data);
              var obj = JSON.parse(str);

              var destination_lat = obj.results[0].geometry.location.lat;
              var destination_lng = obj.results[0].geometry.location.lng;

              this.to = destination_lat+" , "+destination_lng;
              // console.log("destination:",destination);

              this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+this.store_address+'&key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0')
                .subscribe(data => {
                  var str = JSON.stringify(data);
                  var obj = JSON.parse(str);

                  var origin_lat = obj.results[0].geometry.location.lat;
                  var origin_lng = obj.results[0].geometry.location.lng;

                  this.from = origin_lat+" , "+origin_lng;;
                  console.log("origin",this.from , "destination:",this.to);

                  var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(origin_lat, origin_lng), new google.maps.LatLng(destination_lat, destination_lng));

                  var distance_in_km = distance / 1000;
                  var rounded_distance = Math.round(distance_in_km * 100) / 100;

                  console.log("distance:",rounded_distance, "KM");

                  var eta = rounded_distance / 11;
                  var rounded_eta = Math.round(eta * 100) / 100;

                  var str_eta = rounded_eta.toString();
                  console.log("eta:",rounded_eta);

                  var hours   = parseInt(str_eta.substring(0, 1), 10);
                  console.log("Hour:", hours);

                  var mins_str   = "0."+str_eta.substring(2, 4);
                  var mins = parseFloat(mins_str);

                  var minutes = Math.round(mins * 60);

                  console.log("Minutes:", minutes);

                  if(rounded_distance > 1){

                    /*console.log((rounded_distance*1000)+" METERS");
                    this.display_distance = (rounded_distance*1000)+" METERS";
                    var km = rounded_distance.toString().substring(0, 1);
                    var m = rounded_distance.toString().substring(2, 4);
                    var m_float = parseFloat(m);
                    var meters = m_float * 1000;*/

                    this.display_distance = rounded_distance+" km";

                  }else if(rounded_distance < 1){
                    console.log((rounded_distance*1000)+" m");
                    this.display_distance = (rounded_distance*1000)+" m";
                  }

                  if(hours == 0){
                    this.eta = "("+minutes +" min)";
                  }else if(hours > 0){
                    this.eta = "("+hours + " h & "+minutes +" min)";
                  }

                });


            });
          // Math.round(num * 100) / 100 https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=World%20Trade%20Exchange%20Building,%20Juan%20Luna%20Street,%20Binondo,%20Manila,%20Metro%20Manila,%20Philippines&destinations=Lucky%20Chinatown%20Mall,%20Lachambre%20Street,%20Binondo,%20Manila,%20Metro%20Manila,%20Philippines&key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0



          this.order_req = obj.order;
          this.orderId = obj.order[0].ORDER_ID;
          this.orderDate = obj.order[0].DATE_ADDED;
          this.order_status_req = obj.order[0].ORDER_STATUS;
          this.address = obj.order[0].DELIVERY_ADDRESS;
          this.deliveryMode = obj.order[0].DELIVERY_OPTION;
          this.delivery_date = obj.order[0].DELIVERY_DATE;
          this.paymentMode = obj.order[0].PAYMENT_OPTION;
          this.cardNumber = obj.order[0].card_number;


        });
      });

      if (this.gift == 0){
        this.title = "Your Order";
        this.storeState = true;
        this.inProcess = true;

      }else{
        this.title = "Your Gift";
        this.storeState = true;
        this.inProcess = true;
      }

    });
  }


}
