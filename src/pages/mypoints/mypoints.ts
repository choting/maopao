import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MypromosPage } from '../mypromos/mypromos';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the MypointsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mypoints',
  templateUrl: 'mypoints.html',
})
export class MypointsPage implements OnInit {

  myPoints: number = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MypointsPage');
  }

  ngOnInit() {
    this.data.myCurrentPoints.subscribe(val => this.myPoints = val);
  }

  goToPointsStore() {
    this.navCtrl.push(MypromosPage, {
      view: "store"
    });
  }

}
