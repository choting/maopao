import { Component, OnDestroy, OnInit  } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DragulaService, dragula } from 'ng2-dragula';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the GroceryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-grocery',
  templateUrl: 'grocery.html',
})
export class GroceryPage implements OnDestroy, OnInit {

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;
  
  categories = [
    {id: 1, name: "Chocolates", selected: false},
    {id: 2, name: "Cookies", selected: false},
    {id: 3, name: "Frozen goods", selected: false},
    {id: 4, name: "Instant", selected: false},
    {id: 5, name: "Kitchen spices", selected: false}
  ]

  chocolates = [
    [
      { prodId: 29, grocery_image: "assets/imgs/grocery/grocery_g1.png" },
      { prodId: 30, grocery_image: "assets/imgs/grocery/grocery_g2.png" },
      { prodId: 31, grocery_image: "assets/imgs/grocery/grocery_g3.png" },
      { prodId: 32, grocery_image: "assets/imgs/grocery/grocery_g4.png" },
      { prodId: 33, grocery_image: "assets/imgs/grocery/grocery_g5.png" },
      { prodId: 34, grocery_image: "assets/imgs/grocery/grocery_g6.png" }
    ],
    [
      { prodId: 35, grocery_image: "assets/imgs/grocery/grocery_g7.png" },
      { prodId: 36, grocery_image: "assets/imgs/grocery/grocery_g8.png" },
      { prodId: 37, grocery_image: "assets/imgs/grocery/grocery_g9.png" },
      { prodId: 38, grocery_image: "assets/imgs/grocery/grocery_g10.png" },
      { prodId: 39, grocery_image: "assets/imgs/grocery/grocery_g11.png" },
      { prodId: 40, grocery_image: "assets/imgs/grocery/grocery_g12.png" }
    ],
    [
      { prodId: 41, grocery_image: "assets/imgs/grocery/grocery_g13.png" },
      { prodId: 42, grocery_image: "assets/imgs/grocery/grocery_g14.png" },
      { prodId: 43, grocery_image: "assets/imgs/grocery/grocery_g15.png" },
      { prodId: 44, grocery_image: "assets/imgs/grocery/grocery_g16.png" },
      { prodId: 45, grocery_image: "assets/imgs/grocery/grocery_g17.png" },
      { prodId: 46, grocery_image: "assets/imgs/grocery/grocery_g18.png" }
    ],
    [
      { prodId: 47, grocery_image: "assets/imgs/grocery/grocery_g19.png" },
      { prodId: 48, grocery_image: "assets/imgs/grocery/grocery_g20.png" },
      { prodId: 49, grocery_image: "assets/imgs/grocery/grocery_g21.png" },
      { prodId: 50, grocery_image: "assets/imgs/grocery/grocery_g22.png" },
      { prodId: 51, grocery_image: "assets/imgs/grocery/grocery_g23.png" },
      { prodId: 52, grocery_image: "assets/imgs/grocery/grocery_g24.png" }
    ],
    [
      { prodId: 53, grocery_image: "assets/imgs/grocery/grocery_g25.png" },
      { prodId: 54, grocery_image: "assets/imgs/grocery/grocery_g26.png" },
      { prodId: 55, grocery_image: "assets/imgs/grocery/grocery_g27.png" },
      { prodId: 56, grocery_image: "assets/imgs/grocery/grocery_g28.png" },
      { prodId: 57, grocery_image: "assets/imgs/grocery/grocery_g29.png" },
      { prodId: 58, grocery_image: "assets/imgs/grocery/grocery_g30.png" }
    ],
    [
      { prodId: 29, grocery_image: "assets/imgs/grocery/grocery_g1.png" },
      { prodId: 30, grocery_image: "assets/imgs/grocery/grocery_g2.png" },
      { prodId: 31, grocery_image: "assets/imgs/grocery/grocery_g3.png" },
      { prodId: 32, grocery_image: "assets/imgs/grocery/grocery_g4.png" },
      { prodId: 33, grocery_image: "assets/imgs/grocery/grocery_g5.png" },
      { prodId: 34, grocery_image: "assets/imgs/grocery/grocery_g6.png" }
    ]
  ]

  dropArea = [

  ]

  constructor(public navCtrl: NavController, public navParams: NavParams, private dragulaService: DragulaService, private data: DataserviceProvider) {

    

    dragulaService.setOptions('bag-two', {
      copy: function(el,source) {
        return source.classList.contains('grocery-row')
      },
      revertOnSpill: true,
      accepts: function(el, target, source, sibling) {
        return target.classList.contains('drop-area')
      },
      removeOnSpill: function(el, source) {
        return source.classList.contains('drop-area')
      },
      copySortSource: false
    })

    dragulaService.dropModel.subscribe((value) => {
      let [bag, el, target, source] = value;
      if (bag == "bag-two" && source.classList.contains('grocery-row')) {
        this.addToCart(el.getAttribute('data-id'))
      }
    })

    dragulaService.remove.subscribe((value) => {
      let [e, el, container, source] = value;
      this.removeFromCart(el.getAttribute('data-id'))
    })

  }

  ngOnDestroy() {
    this.dragulaService.destroy('bag-two');
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
  }

  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }

  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) { 
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      } 
    }
    this.data.updateCartCount(--this.cartCount);
  }

}
