import { Component } from '@angular/core';
import { OrdersPage } from '../orders/orders';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfirmorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmorder',
  templateUrl: 'confirmorder.html',
})
export class ConfirmorderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmorderPage');
  }

  goToOrderHistory() {
    this.navCtrl.push(OrdersPage, {
      from: "confirmPage"
    });
  }

}
