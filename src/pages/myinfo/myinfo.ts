import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AddressPage } from '../address/address';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { OrdersPage } from '../orders/orders';

import { AuthPage } from '../auth/auth';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

import { HttpClient } from "@angular/common/http";

import { AlertController } from 'ionic-angular';
import {SearchaddPage} from "../searchadd/searchadd";
import {ProfilePage} from "../profile/profile";
/**
 * Generated class for the MyinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-myinfo',
  templateUrl: 'myinfo.html',
})
export class MyinfoPage implements OnInit {

  firstname: string = null;
  lastname: string = null;
  fullname: string = null;
  email: string = null;
  mobile: string = null;

  fruits = ["apple", "banana", "cherry"];

  saved: boolean = false;
  address = [];
  deliveryAddress:any;

  address2 = [];
  address3 = [];

  password: number = 2301;
  username: string = "fwish";

  loggedIn: boolean = false;

  confirmMessage: string = null;

  logged:any;
  id:any;

  signUp: boolean = false;

  pentry: number = null;
  uentry: string = null;
  deleting = false;
  selectedToDel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private http: HttpClient, public alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    // this.getCity();
    console.log('ionViewDidLoad MyinfoPage');
    this.checkAuth();
    this.confirmMessage = "";
    this.getAddress();
  }

  pressed(){
    console.log("pressed");
  }
  active(id, selected){
    this.deliveryAddress = selected;
    this.selectedToDel = id;
    this.deleting = true;
    console.log("active..")
  }

  released(){
    console.log("released...");
  }
  del(){
    this.presentDelConfirm();
  }
  selectToDel(id, selectedAddress){
    this.deliveryAddress = selectedAddress;
    this.selectedToDel = id;
    console.log("ID:", id, " ", "ADDRESS: ", selectedAddress);
  }

  presentDelConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Delete Address',
      message: 'Are you sure you want to delete: '+this.deliveryAddress,
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.deleting = false;
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_delete_address.php?id="+this.selectedToDel).subscribe(data => {
              this.deleting = false;
              console.log("deleted id:", this.selectedToDel);

              let currentIndex = this.navCtrl.getActive().index;
              console.log("myinfo index:",currentIndex);

              this.navCtrl.push(MyinfoPage).then(() => {
                this.navCtrl.remove(currentIndex);
              });

            });

          }
        }
      ]
    });
    alert.present();
  }

  checkAuth(){
    this.storage.get('success').then((val) => {
      console.log('LOGIN:', val);
      if (val == 1){

        this.storage.get('firstname').then((val) => {
          // console.log('GLOBAL FIRSTNAME:', val);
          this.firstname = val;
          if (val == null || val == ""){
            this.firstname = " ";

          }else{
            this.firstname = val;
            console.log('FIRSTNAME:',  this.firstname);
            this.fullname = this.firstname;
          }
        });
        this.storage.get('lastname').then((val) => {
          // console.log('GLOBAL LASTNAME:', val);
          this.lastname = val;
          if (val == null || val == ""){
            this.lastname = "";

          }else{
            this.lastname = val;
            console.log('LASTNAME:',  this.lastname);
            this.fullname = this.fullname + " " + this.lastname;
            console.log('FULLNAME: ',  this.fullname);
          }
        });
        this.storage.get('email').then((val) => {
          console.log('GLOBAL EMAIL:', val);
          this.email = val;
        });
        this.storage.get('mobile').then((val) => {
          console.log('GLOBAL MOBILE:', val);
          this.mobile = val;
        });


        this.storage.get('myLat').then((val) => {
          console.log('GLOBAL MYLAT:', val);
        });

        this.storage.get('myLng').then((val) => {
          console.log('GLOBAL MYLNG:', val);
        });

        this.storage.get('customer_id').then((val) => {
          console.log('GLOBAL USER ID', val);
          this.id = val;
        });

      }

    });
  }

  ngOnInit() {
    this.data.currentLoggedIn.subscribe(val => this.loggedIn = val);
    this.data.myCurrentAddress.subscribe(val => this.address = val);
    this.deliveryAddress = this.address[0].value;

  }

  addAddress() {
    this.storage.set('address_id', null);
    this.storage.set('address_type', null);
    this.storage.set('landmark', null);

    this.navCtrl.push(AddressPage);
  }


  goToAddress(val) {

    let currentIndex = this.navCtrl.getActive().index;
    console.log("from myinfo index:",currentIndex);
    console.log("you clicked",val);

    this.navCtrl.push(AddressPage, {
      address: val
    });
  }

  save(fn, ln, mobile, email) {

    var Hotline = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
    var mobilePh = /^(\+?\d{2}?\s?\d{3}\s?\d{3}\s?\d{4})|([0]\d{3}\s?\d{3}\s?\d{4})$/;
    var password = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,40})$/;
    var validationEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if(this.firstname == "" || this.lastname == "" || this.mobile == "" || this.email == "" || this.firstname == null || this.lastname == null || this.mobile == null || this.email == null)
    {
      console.log("You must fill in all of the fields.");
      this.confirmMessage = "You must fill in all of the fields.";
    }
    else if (!this.mobile.match(mobilePh))
    {
      this.confirmMessage = "Invalid Phone Number!";
      console.log("Invalid Phone Number!");
    }
    else if (!this.email.match(validationEmail))
    {
      this.confirmMessage = "Invalid Email Address!";
      console.log("Invalid Email Address!");
    }

    else{
      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_update_customer.php?customer_id='+this.id+'&firstname='+fn+'&lastname='+ln+'&mobile='+mobile+'&email='+email, {})
        .subscribe(data => {
          console.log(data)
          this.storage.set('customer_id', data["customer_id"]);
          this.storage.set('firstname', data["firstname"]);
          this.storage.set('lastname', data["lastname"]);
          this.storage.set('mobile', data["mobile"]);
          this.storage.set('email', data["email"]);
          this.saved = false;
          this.editSuccess();
        })

    }


  }

  logOut() {

    /*this.storage.set('success', 0);
    this.storage.set('customer_id', null);
    this.storage.set('firstname', "");
    this.storage.set('lastname', "");
    this.storage.set('mobile', "");
    this.storage.set('email', "");
    this.storage.set('my_friend_lists', null);
    this.storage.set('my_friend_requests', null);
    this.storage.set('friend_request_count', null);
    this.storage.set('myLat', null);
    this.storage.set('myLng', null);*/
    this.storage.clear();


    this.data.updateLoggedInValue(this.loggedIn = false);
    let currentIndex = this.navCtrl.getActive().index;
    console.log("current index:",currentIndex);

    this.navCtrl.setRoot(ProfilePage);

  }

  editSuccess() {
    const alert = this.alertCtrl.create({
      title: 'SUCCESS!',
      subTitle: 'Your profile has been updated!',
      buttons: [
        {
          text: 'Okay',
          handler: data => {
            console.log('Okay clicked');
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
          }
        }
      ]
    });
    alert.present();
  }

  getCity(){


    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/get_city.php')
      .subscribe(data => {
        // JSON REQUEST
        // console.log(data);

        //complete object converted to viewable string array
        // var myJSON = JSON.stringify(data);

        //inside object country:
        var myJSONObj = JSON.stringify(data["country"]);

        var object = JSON.parse(myJSONObj);
        this.address3 = object;
        console.log("address3 object:",this.address3);


        //complete object converted to viewable string array
        // console.log("stringify:"+myJSON);
        //inside object country:
        // console.log("stringify of object country"+myJSONObj);


        /*console.log("parse:"+JSON.parse(myJSONObj));
        console.log("parse object:"+object);*/

        for (var a = 0; a<object.length; a++){

          var obj = JSON.parse(myJSONObj);
          var arrName = JSON.stringify(obj[a]["name"]);
          var JSONString = JSON.stringify(obj[a]);
          this.address2.push(JSONString);
          console.log("parse & stringify:"+arrName);

        }

        console.log("JSONArray REQUEST:"+this.address2);



      })
  }

  getAddress(){

    this.storage.get('customer_id').then((val) => {

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_address.php?customer_id='+val)
        .subscribe(data => {

          var myJSONObj = JSON.stringify(data["address"]);

          var object = JSON.parse(myJSONObj);
          this.address3 = object;

          // console.log(myJSONObj);
          console.log(data);


        })

    });

  }


  /*itemPressed(){
    this.saved = true;
    console.log("Item Pressed!");
  }

  itemTapped(){
    this.saved = false;
    console.log("Item Tapped!!!");
  }*/

}
