import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderhistoryPage } from '../orderhistory/orderhistory';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";

/**
 * Generated class for the GiftsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gifts',
  templateUrl: 'gifts.html',
})
export class GiftsPage implements OnInit {

  orders = [];
  friends = [];

  mygift_table:any;
  arr_mygift:any;

  yourgift_table:any;
  arr_yourgift:any;

  my_id:any;



  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private http: HttpClient) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiftsPage');

    this.storage.get('customer_id').then((customer_id) => {
      this.my_id = customer_id;

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_mygift.php?recipient_id='+ customer_id+'&gift=1' )
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          this.mygift_table = obj;
          console.log(this.mygift_table);


          if(this.mygift_table.success != 3){
            console.log(this.mygift_table.order);
            console.log(this.mygift_table.order.length);

            this.arr_mygift  = this.mygift_table.order;
            console.log("ARRAY:", this.arr_mygift);

            for(var a = 0; a < this.mygift_table.order.length; a++){
              console.log("~");
              console.log(this.mygift_table.order[a].ORDER_ID);
              console.log(this.mygift_table.order[a].DELIVERY_DATE);
              console.log(this.mygift_table.order[a].ORDER_STATUS);
              console.log(this.mygift_table.order[a].first_store_logo);
            }
          }else{
            var null_array = [];
            this.arr_mygift  = null_array;
          }




        });

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_yourgift.php?customer_id='+ customer_id+'&gift=1' )
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          this.yourgift_table = obj;
          console.log(this.yourgift_table);


          if(this.yourgift_table.success != 3){
            console.log(this.yourgift_table.order);
            console.log(this.yourgift_table.order.length);

            this.arr_yourgift  = this.yourgift_table.order;
            console.log("ARRAY:", this.arr_yourgift);

            for(var a = 0; a < this.yourgift_table.order.length; a++){
              console.log("~");
              console.log(this.yourgift_table.order[a].ORDER_ID);
              console.log(this.yourgift_table.order[a].DELIVERY_DATE);
              console.log(this.yourgift_table.order[a].ORDER_STATUS);
              console.log(this.yourgift_table.order[a].first_store_logo);
            }
          }else{
            var null_array = [];
            this.arr_yourgift  = null_array;
          }




        });


    });

  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter GiftsPage');

    /*this.storage.get('customer_id').then((customer_id) => {
      this.my_id = customer_id;

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_mygift.php?recipient_id='+ customer_id+'&gift=1' )
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          this.mygift_table = obj;
          console.log(this.mygift_table);


          if(this.mygift_table.success != 3){
            console.log(this.mygift_table.order);
            console.log(this.mygift_table.order.length);

            this.arr_mygift  = this.mygift_table.order;
            console.log("ARRAY:", this.arr_mygift);

            for(var a = 0; a < this.mygift_table.order.length; a++){
              console.log("~");
              console.log(this.mygift_table.order[a].ORDER_ID);
              console.log(this.mygift_table.order[a].DELIVERY_DATE);
              console.log(this.mygift_table.order[a].ORDER_STATUS);
              console.log(this.mygift_table.order[a].first_store_logo);
            }
          }else{
            var null_array = [];
            this.arr_mygift  = null_array;
          }




        });

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_yourgift.php?customer_id='+ customer_id+'&gift=1' )
        .subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          this.yourgift_table = obj;
          console.log(this.yourgift_table);


          if(this.yourgift_table.success != 3){
            console.log(this.yourgift_table.order);
            console.log(this.yourgift_table.order.length);

            this.arr_yourgift  = this.yourgift_table.order;
            console.log("ARRAY:", this.arr_yourgift);

            for(var a = 0; a < this.yourgift_table.order.length; a++){
              console.log("~");
              console.log(this.yourgift_table.order[a].ORDER_ID);
              console.log(this.yourgift_table.order[a].DELIVERY_DATE);
              console.log(this.yourgift_table.order[a].ORDER_STATUS);
              console.log(this.yourgift_table.order[a].first_store_logo);
            }
          }else{
            var null_array = [];
            this.arr_yourgift  = null_array;
          }




        });


    });*/

  }

  ngOnInit() {
    this.data.currentOrders.subscribe(val => this.orders = val);
    this.data.currentFriends.subscribe(val => this.friends = val);
    this.orders.forEach(function(order, index) {
      // var store = this.stores.find(s => s.storeId == order.storeId);

      if (order.deliveryState < 4) {

        var row = this.friends.find(row => row.some(friend => friend.id == order.recepientId));
        var f = row.find(f => f.id == order.recepientId);
        order.recepientName = f.name;
        order.logo = f.image;

      } else if (order.deliveryState > 3 && order.deliveryState < 8) {

        var row = this.friends.find(row => row.some(friend => friend.id == order.senderId));
        var f = row.find(f => f.id == order.senderId);
        order.senderName = f.name;
        order.logo = f.image;

      }

    }.bind(this));
  }

  goToOrderHistory(id) {
    /*this.navCtrl.push(OrderhistoryPage, {
      orderId: id
    });*/

    /*$order["gift"] = $row["gift"];
    $order["gift_status"] = $row["gift_status"];
    $order["firstname"] = $row2["firstname"];
    $order["lastname"] = $row2["lastname"];*/
    console.log(id);

    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_select_order.php?order_id='+id)
      .subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        console.log(obj);

        console.log("orderid:",obj.order[0].ORDER_ID);
        this.storage.set('order.ORDER_ID', obj.order[0].ORDER_ID);

        console.log("from:",obj.order[0].CUSTOMER_ID);
        this.storage.set('order.CUSTOMER_ID', obj.order[0].CUSTOMER_ID);

        console.log("to:",obj.order[0].RECIPIENT_ID);
        this.storage.set('order.RECIPIENT_ID', obj.order[0].RECIPIENT_ID);

        console.log("status:",obj.order[0].ORDER_STATUS);
        this.storage.set('order.ORDER_STATUS', obj.order[0].ORDER_STATUS);

        console.log("gift:",obj.order[0].gift);
        this.storage.set('order.gift', obj.order[0].gift);

        console.log("giftstatus:",obj.order[0].gift_status);
        this.storage.set('order.gift_status', obj.order[0].gift_status);

        console.log("gift_fname:",obj.order[0].firstname);
        this.storage.set('order.firstname', obj.order[0].firstname);

        console.log("gift_lname:",obj.order[0].lastname);
        this.storage.set('order.lastname', obj.order[0].lastname);

        this.navCtrl.push(OrderhistoryPage);

      });



    // console.log("order history clicked id:",id);
    /*this.storage.set('order_history_id', id);
    this.navCtrl.push(OrderhistoryPage);*/
  }

}
