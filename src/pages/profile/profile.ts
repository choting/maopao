import { Component, OnInit } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';

import { MyinfoPage } from '../myinfo/myinfo';
import { MypointsPage } from '../mypoints/mypoints';
import { FriendsPage } from '../friends/friends';
import { GiftsPage } from '../gifts/gifts';
import { MypromosPage } from '../mypromos/mypromos';
import { SalePage } from '../sale/sale';
import { ContactusPage } from '../contactus/contactus';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import { AuthPage } from '../auth/auth';

import { Storage } from '@ionic/storage';
import {AddressPage} from "../address/address";
import {HttpClient} from "@angular/common/http";
import {Network} from "@ionic-native/network";


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

  openModal = false;
  loggedIn: boolean = false;
  logged:any;
  id:any;


  customer_id:any;
  firstname:any;
  lastname:any;
  fullname:any;
  mobile:any;
  email:any;

  friendsReq:any;
  friendsReqCount:any;
  friend_lists:any;
  friend_lists_count:any;
  login_success:any;


  warning:any;


  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private http: HttpClient, public network: Network) {

  }

  ionViewDidLoad() {

    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidLoad ProfilePage');
    console.log("==========================");
    this.checkLogin();
    // this.getCurrentLocation();
    // this.checkLogin();

  }

  ionViewDidEnter(){

      console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
      console.log('ionViewDidEnter ProfilePage');
      console.log("==========================");
    this.storage.get("success").then( log => {
      if (log == 1) {

          this.storage.get('firstname').then((firstname) => {
            this.firstname = firstname;
            this.storage.get('lastname').then((lastname) => {
              this.lastname = lastname;
              this.storage.get('email').then((email) => {
                this.fullname = this.firstname + " " + this.lastname;
                this.email = email;
                this.storage.get('mobile').then((mobile) => {
                  this.mobile = mobile;
                });
              });
            });
          });
      }
    });

  }


  checkLogin(){
    this.storage.get("success").then( log => {
      if(log == 1){

        this.storage.get('firstname').then((firstname) => {
          this.firstname = firstname;
          this.storage.get('lastname').then((lastname) => {
            this.lastname = lastname;
            this.storage.get('email').then((email) => {
              this.fullname = this.firstname + " " + this.lastname;
              this.email = email;
              this.storage.get('mobile').then((mobile) => {
                this.mobile = mobile;
              });
            });
          });
        });

        this.data.updateLoggedInValue(this.loggedIn = true);
        console.log("LOGGED ON");

        this.storage.get('customer_id').then((customer_id) => {
          this.customer_id = customer_id;
          console.log("USER ID:",customer_id);
          this.getFriendRequests();
          this.getFriendLists();
          // this.getAccountInfo();
          // this.viewFriendsRequests();
          // this.viewFriendLists();

        });

      }else{
        console.log("LOGGED OUT");
        this.data.updateLoggedInValue(this.loggedIn = false);
      }

    })
  }



  getFriendRequests(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friend_request.php?friend_request_to="+this.customer_id)
      .subscribe(fr => {
        var str = JSON.stringify(fr);
        var obj = JSON.parse(str);

        console.log("FRIEND REQUEST STATUS:");
        console.log(fr);

        if(obj.success == 3){
          console.log("FRIEND REQUEST: 0");

        }else if(obj.success == 0){
          console.log("NO RESULT.");
        }
        else{
          this.storage.set('my_friend_requests', obj);
          console.log("FRIEND REQUEST STORED.");
          this.friendsReqCount = obj.friend_request.length;
          console.log("COUNT:", this.friendsReqCount);
        }
      });
  }

  getFriendLists(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.customer_id)
      .subscribe(fl => {
        var str = JSON.stringify(fl);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("FRIEND LIST STATUS:");
          console.log("FRIENDS: 0");
          console.log(fl);

        }else if(obj.success == 0){
          console.log("NO RESULT.");
        }
        else{
          console.log("FRIEND LIST STATUS:");
          console.log("FRIENDS COUNT:", obj.friend_request.length);
          console.log(fl);
          this.storage.set('my_friend_lists', obj);

        }

      });
  }

  ngOnInit() {
    this.data.currentLoggedIn.subscribe(val => this.loggedIn = val)
  }

  goToMyInfo() {

    this.storage.get('success').then((val) => {
      if(val == 1){
        this.data.updateLoggedInValue(this.loggedIn = true);
        this.navCtrl.push(MyinfoPage);

      }else if(val == 0 || val == null){
        this.navCtrl.push(AuthPage, {
          from: "profile"
        });

      }

    });

  }

  goToMyPoints() {
    this.navCtrl.push(MypointsPage);
  }

  goToFriends() {
    this.navCtrl.push(FriendsPage);
  }

  goToGifts() {
    this.navCtrl.push(GiftsPage);
  }

  goToMyPromos() {
    this.navCtrl.push(MypromosPage);
  }

  goToSale() {
    this.navCtrl.push(SalePage);
  }

  goToContact() {
    this.navCtrl.push(ContactusPage);
  }

}
