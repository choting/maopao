import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the MypromosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mypromos',
  templateUrl: 'mypromos.html',
})
export class MypromosPage implements OnInit {

  view = null;
  storePromos: any = null;
  stores: any = null;
  selectedCount: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
    if (this.navParams.get('view')) {
      this.view = this.navParams.get('view');
    } else {
      this.view = "availed";
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MypromosPage');
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentStorePromos.subscribe(val => this.storePromos = val);
    this.storePromos.forEach((promo) => {
      let store = this.stores.find(s => s.storeId == promo.storeId);
      promo.logo = store.logo;
      promo.storeName = store.name;
    });
  }

  segmentChanged($event) {
    
  }

  toggleSelect(val) {
    let promo = this.storePromos.find(p => p.promoId == val);
    if (promo.selected) {
      promo.selected = false;
      this.selectedCount--;
    } else {
      promo.selected = true;
      this.selectedCount++;
    }
  }

  buyPromos() {
    this.storePromos.forEach(promo => {
      if (promo.selected) {
        this.data.updatePromos(promo.promoId, (promo.bought = true));
        promo.selected = false;
        this.selectedCount--;
      }
    });
  }

}
