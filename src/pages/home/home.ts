import { Component, OnInit } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import { ImagemapPage } from '../imagemap/imagemap';
import { VendingmachinePage } from '../vendingmachine/vendingmachine';
import { GroceryPage } from '../grocery/grocery';
import { ProductPage } from '../product/product';
import { StorePage } from '../store/store';
import { CategoriesPage } from '../categories/categories';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import {HttpClient} from "@angular/common/http";
import {Network} from "@ionic-native/network";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {

  //banner slides
  slides = [
    "assets/imgs/banners/banner_1.jpg",
    "assets/imgs/banners/banner_2.jpg",
    "assets/imgs/banners/banner_3.jpg",
    "assets/imgs/banners/banner_4.jpg"
  ];

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  openModal: boolean = true;
  goToNextModal: boolean = false;

  loggedIn: boolean = false;

  results: any = null;

  selectedCategoryCount: number = 0;


  customer_id:any;
  firstname:any;
  lastname:any;
  fullname:any;
  mobile:any;
  email:any;

  friendsReq:any;
  friendsReqCount:any;
  friend_lists:any;
  friend_lists_count:any;
  login_success:any;
  warning:any;
  clat:any;
  clng:any;


  constructor
  (private alertCtrl: AlertController,
   public navCtrl: NavController,
   private data: DataserviceProvider,
   private storage: Storage,
   private geo: Geolocation,
   private http:HttpClient,
   public network: Network)
  {


  }

  ionViewDidLoad() {
    this.network.onDisconnect().subscribe(() => {
      this.NoConnection();
    });

    this.network.onConnect().subscribe(() => {
      this.warning.dismiss();
    });

    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log('ionViewDidLoad HomePage');
    console.log("==========================");

    this.getCurrentLocation();
    this.checkLogin();

  }

  NoConnection(){
    this.warning = this.alertCtrl.create({
      title: 'No Internet Connection',
      subTitle: 'Please check your internet connection.',
      enableBackdropDismiss: false
    });
    this.warning.present();
  }

  NoLocation(){
    this.warning = this.alertCtrl.create({
      title: 'Location not found!',
      subTitle: 'Please turn on Location Services.',
      enableBackdropDismiss: false
    });
    this.warning.present();
  }

  checkLogin(){
    this.storage.get("success").then( log => {
      if(log == 1){

        this.data.updateLoggedInValue(this.loggedIn = true);
        console.log("LOGGED ON");

        this.storage.get('customer_id').then((customer_id) => {
          this.customer_id = customer_id;
          console.log("USER ID:",customer_id);
          this.getAccountInfo();
          this.viewFriendsRequests();
          this.viewFriendLists();

        });

      }else{
        console.log("LOGGED OUT");
        this.data.updateLoggedInValue(this.loggedIn = false);
      }

    })
  }

  getFriendRequests(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friend_request.php?friend_request_to="+this.customer_id)
      .subscribe(fr => {
        this.storage.set('my_friend_requests', fr);
        console.log("FRIEND REQUEST STORED.");
    });
  }

  getFriendLists(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.customer_id)
      .subscribe(fr => {
        this.storage.set('my_friend_lists', fr);
        console.log("FRIEND LISTS STORED.");
      });
  }

  viewFriendsRequests(){
    this.storage.get('my_friend_requests').then((fr_req) => {

      if (fr_req == null){
        console.log("NO RESULTS");
      }
      else{

        if (fr_req.success == 3){
          console.log("NO FRIEND REQUEST!");
        }else if (fr_req.success == 0){
          console.log("NO ID, NO FRIEND REQUEST");
        }else{
          console.log("FRIEND REQUESTS:",fr_req);
          console.log("FRIEND REQUEST:",fr_req.friend_request.length);
        }

      }
    });
  }



  viewFriendLists(){
    this.storage.get('my_friend_lists').then((fl) => {

      if (fl == null){
        console.log("NO RESULTS");
      }
      else{

        if (fl.success == 3){
          console.log("NO FRIEND REQUEST!");
        }else if (fl.success == 0){
          console.log("NO ID, NO FRIEND REQUEST");
        }else{
          console.log("FRIENDS:",fl);
          console.log("FRIENDS COUNT:",fl.friend_request.length);
        }

      }
    });
  }

  getCurrentLocation(){
    this.geo.getCurrentPosition().then(pos => {
        this.storage.set("myLat", pos.coords.latitude);
        this.storage.set("myLng", pos.coords.longitude);
    });

    this.storage.get("myLat").then( lat => {
      this.storage.get("myLng").then( lng => {
        console.log("global latitude:",lat);
        console.log("global longitude:",lng);

      });
    });
  }

  getAccountInfo(){

    this.storage.get('firstname').then((firstname) => {
      this.firstname = firstname;
      this.storage.get('lastname').then((lastname) => {
        this.lastname = lastname;
        this.storage.get('email').then((email) => {
          this.fullname = this.firstname+ " "+this.lastname;
          this.email = email;
          this.storage.get('mobile').then((mobile) => {
            this.mobile = mobile;


            console.log(this.fullname);
            console.log(this.firstname);
            console.log(this.lastname);
            console.log(this.email);
            console.log(this.mobile);
            this.getFriendRequests();
            this.getFriendLists();
          });
        });
      });
    });

  }


  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
  }

  goToMap() {
    this.navCtrl.push(ImagemapPage);
  }
  goToVendingMachine() {
    this.navCtrl.push(VendingmachinePage);
  }
  goToGrocery() {
    this.navCtrl.push(GroceryPage);
  }
  goToProduct(id) {
    this.navCtrl.push(ProductPage, {
      prodId: id
    });
  }
  goToStore(id) {
    this.navCtrl.push(StorePage, {
      storeId: id
    });
  }

  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }

  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) {
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      }
    }
    this.data.updateCartCount(--this.cartCount);
  }

  getItems(ev: any) {
    const val = ev.target.value;
    if (val) {
      this.results = this.products.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
      this.results = this.results.concat(this.stores.filter((store) => {
        return (store.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      }));
    } else if (val == '') {
      this.results = null;
    }
  }

  cancelItems(ev: any) {
    this.results = null;
  }

  categories = [
    [
      { id: 1,    name: "Beef",         image: "assets/imgs/categories/beef_icon.png",          selected: false},
      { id: 2,    name: "Dimsum",       image: "assets/imgs/categories/dimsum_icon.png",        selected: false},
      { id: 3,    name: "Coffee",       image: "assets/imgs/categories/coffee_icon.png",        selected: false},
      { id: 4,    name: "Chicken",      image: "assets/imgs/categories/chicken_icon.png",       selected: false}
    ],
    [
      { id: 5,    name: "Duck",         image: "assets/imgs/categories/duck_icon.png",          selected: false},
      { id: 6,    name: "Beef",         image: "assets/imgs/categories/eggs_icon.png",          selected: false},
      { id: 7,    name: "Fried",        image: "assets/imgs/categories/fried_icon.png",         selected: false},
      { id: 8,    name: "Fruits",       image: "assets/imgs/categories/fruits_icon.png",        selected: false}
    ],
    [
      { id: 9,    name: "Hot Pot",      image: "assets/imgs/categories/hotpot_icon.png",        selected: false},
      { id: 10,   name: "Juice",        image: "assets/imgs/categories/juice_icon.png",         selected: false},
      { id: 11,   name: "Liquor",       image: "assets/imgs/categories/liquor_icon.png",        selected: false},
      { id: 12,   name: "Noodles",      image: "assets/imgs/categories/noodles_icon.png",       selected: false}
    ],
    [
      { id: 13,   name: "Pork",         image: "assets/imgs/categories/pork_icon.png",          selected: false},
      { id: 14,   name: "Rice Meal",    image: "assets/imgs/categories/ricemeal_icon.png",      selected: false},
      { id: 15,   name: "Sea Food",     image: "assets/imgs/categories/seafood_icon.png",       selected: false},
      { id: 16,   name: "Sio Pao",      image: "assets/imgs/categories/siopao_icon.png",        selected: false}
    ],
    [
      { id: 17,   name: "Soda",         image: "assets/imgs/categories/soda_icon.png",          selected: false},
      { id: 18,   name: "Soup",         image: "assets/imgs/categories/soup_icon.png",          selected: false},
      { id: 19,   name: "Tea",          image: "assets/imgs/categories/tea_icon.png",           selected: false},
      { id: 20,   name: "Vegetables",   image: "assets/imgs/categories/vegetables_icon.png",    selected: false}
    ]
  ]

  selectingCategories(id) {
    var row = this.categories.find(row => row.some(c => c.id == id));
    var category = row.find(c => c.id == id);

    if (category.selected) {
      category.selected = false;
      this.selectedCategoryCount--;
    } else {
      category.selected = true;
      this.selectedCategoryCount++;
    }
  }

  goToCategories() {
    this.navCtrl.push(CategoriesPage);
  }


}
