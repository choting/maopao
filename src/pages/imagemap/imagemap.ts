import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ImagemapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-imagemap',
  templateUrl: 'imagemap.html',
})
export class ImagemapPage {

  storeMap = "assets/imgs/stores_map/manila_day.jpg";

  manilaShown:boolean = true;

  today = new Date();
  hour = this.today.getHours();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if (this.hour >= 18) {  
      this.storeMap = "assets/imgs/stores_map/manila_night.jpg";
    }
  }

  showMakati() {
    if (this.hour >= 18) {
      this.storeMap = "assets/imgs/stores_map/makati_night.jpg";
    } else {
      this.storeMap = "assets/imgs/stores_map/makati_day.jpg";
    }
    this.manilaShown = false;
  }

  showManila() {
    if (this.hour >= 18) {
      this.storeMap = "assets/imgs/stores_map/manila_night.jpg";
    } else {
      this.storeMap = "assets/imgs/stores_map/manila_day.jpg";
    }
    this.manilaShown = true;
  }

}
