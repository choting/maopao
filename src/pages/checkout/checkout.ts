import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrdersPage } from '../orders/orders';
import { ConfirmorderPage } from '../confirmorder/confirmorder';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";
import {AddressPage} from "../address/address";
import {MyinfoPage} from "../myinfo/myinfo";

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  storeState: boolean = true;
  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  friends = [];
  friendSelected = null;
  friendImage = null;
  friendName = null;

  address = [];


  deliveryAddress = [];
  deliveryMode = "asap";
  paymentMode = null;
  deliveryDate = null;

  cardName = null;
  cardNumber = null;
  cvv = null;
  cardExpiration = null;

  openModal:boolean = false;

  theme = "primary";

  grandTotal = 0;

  orders = [];

  promoShown: boolean = false;
  totalDiscount: number = 0;

  storePromos = [];
  availablePromos = [];
  checkoutPromos = null;
  cusId:any;
  myDeliveryAddress:any;

  friend_lists:any;
  customer_id:any;
  recipientName:any;
  customer_id_checkout:any;
  selectedRow:any;

  checkout_address:any;
  checkout_delivery_type:any = 'asap';
  checkout_delivery_date:any;
  checkout_payment_type:any;
  checkout_product:any;

  testvar = 0;
  datetime_now:any;
  primary_key:any;


  orders_table:any;
  order_id:any;
  recipient_id:any;
  minDate:any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private http: HttpClient,) {


    // set friend recepient
    this.stores = navParams.get('stores');
    this.products = navParams.get('products');

    this.stores.forEach(store => {
      this.grandTotal += (store.cartTotal + store.deliveryFee);
    });

    storage.get('customer_id').then((customerId) => {
      storage.get('recipient_id').then((recipientId) => {

      this.recipient_id = recipientId;
      console.log('id:', customerId);
      this.cusId = customerId;


    this.getAddress();
    this.storage.get("check_out_friend").then((val) => {
      this.storage.get("check_out_friend_name").then((name) => {

      if (val != null){
        this.recipientName = name;
        this.paymentMode = null;
        this.friendSelected = val;
        this.myFriends();
      }

      });
    });

    this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_all_order.php')
      .subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        this.orders_table = obj;

        console.log('CHECK ORDER:',obj);
        if(obj.success == 3){
          this.primary_key = null;
          console.log("PRIMARY KEY:", this.primary_key);
        }else{
          this.primary_key = obj.order.length;
          var x = this.primary_key - 1;

          console.log('length:',obj.order.length);
          console.log('recent order id:',obj.order[x].ORDER_ID);
          console.log("PRIMARY KEY:", this.primary_key+1);
        }

      });
    });
    });
  }

  setClickedRow(index, f_id){
    this.customer_id_checkout = f_id;
    this.selectedRow = index;
    console.log("clicked!", this.customer_id_checkout);
  }




  ngOnInit() {
    this.data.currentStoreState.subscribe(val => this.storeState = val);
    //if store is closed, change theme to red
    if(!this.storeState) {
      this.theme = "red-color";
    }
    this.data.currentOrders.subscribe(val => this.orders = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
    this.data.currentFriendSelected.subscribe(val => this.friendSelected = val);
    // if friend is recepient
    if (this.friendSelected !== null) {
      /*this.paymentMode = "creditCard";
      // get friend list, for allowing user to choose another friend if ever
      this.data.currentFriends.subscribe(val => this.friends = val);

      // get details of current selected friend, to show in checkout
      let row = this.friends.find(row => row.some(friend => friend.id == this.friendSelected));
      let friend = row.find(f => f.id == this.friendSelected);
      this.friendName = friend.name;
      this.friendImage = friend.image;

      // load address of friends
      this.data.theirCurrentAddress.subscribe(val => this.address = val);
      this.address =  [ this.address.find(a => a.id == friend.id) ];*/
    } else {
      this.data.myCurrentAddress.subscribe(val => this.address = val);
    }
    this.deliveryAddress = this.address[0].value;

    this.data.currentCheckoutPromos.subscribe(val => this.checkoutPromos = val);
    console.log(this.checkoutPromos)

  }





  increaseProduct(id) {
    let product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    let store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount);
    this.grandTotal += product.price;
  }




  decreaseProduct(id) {
    let product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    let store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) {
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      }
      this.grandTotal -= (product.price + store.deliveryFee);
      this.stores.splice(this.stores.indexOf(store), 1);
    } else if ((store.cartTotal > 0) && (store.inCheckout == true)) {
      this.grandTotal -= product.price;
    }
    this.data.updateCartCount(--this.cartCount);
  }




  setRecepient(id) {

    let row = this.friends.find(row => row.some(friend => friend.id == id));
    let f = row.find(f => f.id == id);
    this.data.updateFriends(id, (f.set = true));
    this.friendName = f.name;
    this.friendImage = f.image;

    if (this.friendSelected !== null) {
      let row = this.friends.find(row => row.some(friend => friend.id == this.friendSelected));
      let pf = row.find(f => f.id == this.friendSelected);
      this.data.updateFriends(pf.id, (pf.set = false));
    }

    this.data.setCurrentFriendSelected(this.friendSelected = id);

  }




  removeRecepient(id) {

    let row = this.friends.find(row => row.some(friend => friend.id == id));
    let friend = row.find(f => f.id == id);
    this.data.updateFriends(id, (friend.set = false));

  }


  selectAddress(address){
    console.log(address);
    this.checkout_address = address;
  }
  selectTypeOfDelivery(time){
      this.checkout_delivery_type = time;
      console.log(this.checkout_delivery_type);
  }

  selectTypeOfPayment(payment){
    this.checkout_payment_type = payment;
    console.log(this.checkout_payment_type);
  }
  updateCheckout(val) {
    console.log(val);
    console.log("PREVIOUS IN CHECKOUT:",this.checkoutCount);
    ++this.testvar;
    console.log("REMOVED IN CHECKOUT:",this.testvar);

    if(this.testvar == this.checkoutCount){
      let currentIndex = this.navCtrl.getActive().index;
      this.navCtrl.remove(currentIndex);
    }

  }


  showCompletion() {

    if(this.recipient_id != this.cusId){

      if(this.checkout_delivery_type == 'advance'){
        this.dateNow();
        var hours   = parseInt(this.deliveryDate.substring(11, 13), 10),
          minutes = this.deliveryDate.substring(14, 16),
          ampm    = 'AM';

        if (hours == 12) {
          ampm = 'PM';
        } else if (hours == 0) {
          hours = 12;
        } else if (hours > 12) {
          hours -= 12;
          ampm = 'PM';
        }

        var time = hours + ':' + minutes + ' ' + ampm;
        var year = this.deliveryDate.substring(0, 4);
        var month = this.deliveryDate.substring(5, 7);
        var day = this.deliveryDate.substring(8, 10);

        this.checkout_delivery_date = month+"/"+day+"/"+year+ " - "+ time;
      }
      else if (this.checkout_delivery_type == 'asap') {
        this.dateNow();
        this.checkout_delivery_date = this.datetime_now;
      }
      var sub_id = this.checkout_delivery_date.substring(6, 10)+this.checkout_delivery_date.substring(0, 2)+this.checkout_delivery_date.substring(3, 5);
      if(this.primary_key == null)
      {
        this.order_id = parseInt(sub_id+1);
      }
      else{
        this.order_id = parseInt(sub_id+(this.primary_key+1));
      }

      console.log("YOU WILL SEND THIS GIFT TO A FRIEND:",this.recipient_id);

      // orders table
      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order.php?id=&order_id='+ this.order_id +'&customer_id='+this.cusId+'&recipient_id='+this.recipient_id+'&delivery_option='+this.checkout_delivery_type+'&delivery_date='+this.checkout_delivery_date+'&delivery_address='+this.checkout_address+'&payment_option='+this.checkout_payment_type+'&grand_total='+this.grandTotal+'&order_status=9&date_added='+this.datetime_now+'&card_number='+this.cardNumber+'&card_name='+this.cardName+'&card_cvv='+this.cvv+'&card_expiration='+this.cardExpiration+'&first_store_logo='+this.stores[0].logo+'&gift='+1+'&gift_status=').subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        console.log(obj);
      });

      // gifts table
      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_gift.php?order_id='+this.order_id+'&gift_from='+this.cusId+'&gift_to='+this.recipient_id).subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        console.log(obj);
      });

      for (var a=0;a<this.stores.length;a++){
        console.log("=======STORE=======");
        if(this.stores[a].inCheckout == true){
          var grand_total = this.stores[a].cartTotal+this.stores[a].deliveryFee;

          console.log(this.stores[a].storeId);
          console.log(this.stores[a].state);
          console.log(this.stores[a].name);
          console.log(this.stores[a].rating);
          console.log(this.stores[a].logo);
          console.log(this.stores[a].address);
          console.log(this.stores[a].contacts);
          console.log(this.stores[a].openHours);
          console.log(this.stores[a].distance);
          console.log(this.stores[a].speed);
          console.log(this.stores[a].viewCount);
          console.log(this.stores[a].orderCount);
          console.log(this.stores[a].inCheckout);
          console.log(this.stores[a].cartTotal);
          console.log(this.stores[a].deliveryFee);
          console.log(grand_total);
          this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_store.php?id=&order_id='+ this.order_id +'&storeId='+this.stores[a].storeId+'&state='+this.stores[a].state+'&name='+this.stores[a].name+'&rating='+this.stores[a].rating+'&logo='+this.stores[a].logo+'&address='+this.stores[a].address+'&contacts='+this.stores[a].contacts+'&openHours='+this.stores[a].openHours+'&distance='+this.stores[a].distance+'&speed='+this.stores[a].speed+'&viewCount='+this.stores[a].viewCount+'&orderCount='+this.stores[a].orderCount+'&inCheckout='+this.stores[a].inCheckout+'&cartTotal='+this.stores[a].cartTotal+'&deliveryFee='+this.stores[a].deliveryFee+'&date_added='+this.datetime_now+'&customer_id='+this.cusId+'&grand_total='+grand_total).subscribe(data => {
            var str = JSON.stringify(data);
            var obj = JSON.parse(str);
            console.log(obj);
          });

          console.log("======PRODUCTS======");
          for(var c in this.products) {
            if(this.stores[a].storeId == this.products[c].storeId){

              console.log(this.products[c].prodId);
              console.log(this.products[c].storeId);
              console.log(this.products[c].name);
              console.log(this.products[c].rating);
              console.log(this.products[c].image);
              console.log(this.products[c].vending_image);
              console.log(this.products[c].price);
              console.log(this.products[c].discountedPrice);
              console.log(this.products[c].quantity);
              console.log(this.products[c].quantityPrice);
              console.log(this.products[c].inFavorites);
              console.log(this.products[c].toRemove);
              console.log(this.products[c].removing);
              this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_product.php?id=&order_id='+ this.order_id +'&prodId='+this.products[c].prodId+'&storeId='+this.products[c].storeId+'&customer_id='+this.cusId+'&name='+this.products[c].name+'&rating='+this.products[c].rating+'&image='+this.products[c].image+'&price='+this.products[c].price+'&discountedPrice='+this.products[c].discountedPrice+'&quantity='+this.products[c].quantity+'&quantityPrice='+this.products[c].quantityPrice+'&inFavorites='+this.products[c].inFavorites+'&toRemove='+this.products[c].toRemove+'&removing='+this.products[c].removing+'&date_added='+this.datetime_now).subscribe(data => {
                var str = JSON.stringify(data);
                var obj = JSON.parse(str);
                console.log(obj);
              });


            }
          }
        }

      }


      console.log("ORDER ID:",this.order_id);
      console.log("======~~~~~~~~======");
      console.log("CARD NAME:",this.cardName);
      console.log("CARD NUMBER:",this.cardNumber);
      console.log("CARD CVV:",this.cvv);
      console.log("CARD EXPIRATION:",this.cardExpiration);

      console.log("DELIVERY OPTION:", this.checkout_delivery_type);
      console.log("DATE:", this.checkout_delivery_date);
      console.log("ADDRESS:", this.checkout_address);
      console.log("PAYMENT OPTION:", this.checkout_payment_type);
      console.log("GRAND TOTAL P:",this.grandTotal);
      console.log("DATE ADDED:",this.datetime_now);
      console.log("CHECKED OUT");
      this.navCtrl.push(ConfirmorderPage);


    }
    else if(this.checkout_address == null){
      alert("Please select an address!");
    }
    else if(this.checkout_payment_type == null){
      alert("Please select a payment option!");
    }
    else if(this.checkout_payment_type == 'creditCard'){

      if(this.cardName == null){
        alert("Please Fill-up Credit Card Name!");
      }else if(this.cardNumber == null){
        alert("Please Fill-up Credit Card Number!");
      }else if(this.cardExpiration == null){
        alert("Please Fill-up Credit Card Expiration!");
      }else if(this.cvv == null){
        alert("Please Fill-up Credit Card Cvv!");
      }else{
        if(this.checkout_delivery_type == 'advance'){
          this.dateNow();
          var hours   = parseInt(this.deliveryDate.substring(11, 13), 10),
            minutes = this.deliveryDate.substring(14, 16),
            ampm    = 'AM';

          if (hours == 12) {
            ampm = 'PM';
          } else if (hours == 0) {
            hours = 12;
          } else if (hours > 12) {
            hours -= 12;
            ampm = 'PM';
          }

          var time = hours + ':' + minutes + ' ' + ampm;
          var year = this.deliveryDate.substring(0, 4);
          var month = this.deliveryDate.substring(5, 7);
          var day = this.deliveryDate.substring(8, 10);

          this.checkout_delivery_date = month+"/"+day+"/"+year+ " - "+ time;

        }
        else if (this.checkout_delivery_type == 'asap') {
          this.dateNow();
          this.checkout_delivery_date = this.datetime_now;
        }

        var sub_id = this.checkout_delivery_date.substring(6, 10)+this.checkout_delivery_date.substring(0, 2)+this.checkout_delivery_date.substring(3, 5);
        if(this.primary_key == null)
        {
          this.order_id = parseInt(sub_id+1);
        }
        else{
          this.order_id = parseInt(sub_id+(this.primary_key+1));
        }

        this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order.php?id=&order_id='+ this.order_id +'&customer_id='+this.cusId+'&recipient_id='+this.recipient_id+'&delivery_option='+this.checkout_delivery_type+'&delivery_date='+this.checkout_delivery_date+'&delivery_address='+this.checkout_address+'&payment_option='+this.checkout_payment_type+'&grand_total='+this.grandTotal+'&order_status=1&date_added='+this.datetime_now+'&card_number='+this.cardNumber+'&card_name='+this.cardName+'&card_cvv='+this.cvv+'&card_expiration='+this.cardExpiration+'&first_store_logo='+this.stores[0].logo+'&gift='+'&gift_status=').subscribe(data => {
          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          console.log(obj);
        });

        for (var a=0;a<this.stores.length;a++){
          console.log("=======STORE=======");
          if(this.stores[a].inCheckout == true){
            var grand_total = this.stores[a].cartTotal+this.stores[a].deliveryFee;
            console.log(this.stores[a].storeId);
            console.log(this.stores[a].state);
            console.log(this.stores[a].name);
            console.log(this.stores[a].rating);
            console.log(this.stores[a].logo);
            console.log(this.stores[a].address);
            console.log(this.stores[a].contacts);
            console.log(this.stores[a].openHours);
            console.log(this.stores[a].distance);
            console.log(this.stores[a].speed);
            console.log(this.stores[a].viewCount);
            console.log(this.stores[a].orderCount);
            console.log(this.stores[a].inCheckout);
            console.log(this.stores[a].cartTotal);
            console.log(this.stores[a].deliveryFee);
            this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_store.php?id=&order_id='+ this.order_id +'&storeId='+this.stores[a].storeId+'&state='+this.stores[a].state+'&name='+this.stores[a].name+'&rating='+this.stores[a].rating+'&logo='+this.stores[a].logo+'&address='+this.stores[a].address+'&contacts='+this.stores[a].contacts+'&openHours='+this.stores[a].openHours+'&distance='+this.stores[a].distance+'&speed='+this.stores[a].speed+'&viewCount='+this.stores[a].viewCount+'&orderCount='+this.stores[a].orderCount+'&inCheckout='+this.stores[a].inCheckout+'&cartTotal='+this.stores[a].cartTotal+'&deliveryFee='+this.stores[a].deliveryFee+'&date_added='+this.datetime_now+'&customer_id='+this.cusId+'&grand_total='+grand_total).subscribe(data => {
              var str = JSON.stringify(data);
              var obj = JSON.parse(str);
              console.log(obj);
            });

            console.log("======PRODUCTS======");
            for(var c in this.products) {
              if(this.stores[a].storeId == this.products[c].storeId){

                console.log(this.products[c].prodId);
                console.log(this.products[c].storeId);
                console.log(this.products[c].name);
                console.log(this.products[c].rating);
                console.log(this.products[c].image);
                console.log(this.products[c].vending_image);
                console.log(this.products[c].price);
                console.log(this.products[c].discountedPrice);
                console.log(this.products[c].quantity);
                console.log(this.products[c].quantityPrice);
                console.log(this.products[c].inFavorites);
                console.log(this.products[c].toRemove);
                console.log(this.products[c].removing);
                this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_product.php?id=&order_id='+ this.order_id +'&prodId='+this.products[c].prodId+'&storeId='+this.products[c].storeId+'&customer_id='+this.cusId+'&name='+this.products[c].name+'&rating='+this.products[c].rating+'&image='+this.products[c].image+'&price='+this.products[c].price+'&discountedPrice='+this.products[c].discountedPrice+'&quantity='+this.products[c].quantity+'&quantityPrice='+this.products[c].quantityPrice+'&inFavorites='+this.products[c].inFavorites+'&toRemove='+this.products[c].toRemove+'&removing='+this.products[c].removing+'&date_added='+this.datetime_now).subscribe(data => {
                  var str = JSON.stringify(data);
                  var obj = JSON.parse(str);
                  console.log(obj);
                });


              }
            }
          }

        }
        console.log("======~~~~~~~~======");
        console.log("CARD NAME:",this.cardName);
        console.log("CARD NUMBER:",this.cardNumber);
        console.log("CARD CVV:",this.cvv);
        console.log("CARD EXPIRATION:",this.cardExpiration);

        console.log("DELIVERY OPTION:", this.checkout_delivery_type);
        console.log("DATE:", this.checkout_delivery_date);
        console.log("ADDRESS:", this.checkout_address);
        console.log("PAYMENT OPTION:", this.checkout_payment_type);
        console.log("GRAND TOTAL P:",this.grandTotal);
        console.log("DATE ADDED:",this.datetime_now);
        console.log("CHECKED OUT");
        this.navCtrl.push(ConfirmorderPage);

      }
    }




    else{


      if(this.checkout_delivery_type == 'advance'){
        this.dateNow();
        // console.log("24hrs format: "+this.deliveryDate);

        var hours   = parseInt(this.deliveryDate.substring(11, 13), 10),
          minutes = this.deliveryDate.substring(14, 16),
          ampm    = 'AM';

        if (hours == 12) {
          ampm = 'PM';
        } else if (hours == 0) {
          hours = 12;
        } else if (hours > 12) {
          hours -= 12;
          ampm = 'PM';
        }

        var time = hours + ':' + minutes + ' ' + ampm;
        var year = this.deliveryDate.substring(0, 4);
        var month = this.deliveryDate.substring(5, 7);
        var day = this.deliveryDate.substring(8, 10);

        this.checkout_delivery_date = month+"/"+day+"/"+year+ " - "+ time;
        // console.log("checkout_delivery_date: ",this.checkout_delivery_date);

      }
      else if (this.checkout_delivery_type == 'asap'){
        this.dateNow();
        /*var date = new Date();

        var hour = date.toLocaleTimeString('en-GB');
        var hrs   = parseInt(hour.substring(0, 2), 10),
          mins = hour.substring(3, 5),
          pmam    = 'AM';

        if (hrs == 12) {
          pmam = 'PM';
        } else if (hrs == 0) {
          hrs = 12;
        } else if (hrs > 12) {
          hrs -= 12;
          pmam = 'PM';
        }

        var time2 = hrs + ':' + mins + ' ' + pmam;

        var years = date.getUTCFullYear();
        var months = date.getMonth()+1;
        if (months < 10) {
          month = "0"+months;
        }

        var days = date.getUTCDate();*/

        // console.log("new Date():"+date);
        // console.log(years+"/"+month+"/"+days+" "+time+" - "+pmam);

        this.checkout_delivery_date = this.datetime_now;

        // console.log("24hr format:"+years+"/"+month+"/"+days+" "+time24);
      }

      for (var i=0;i<this.products.length;i++){
        // dito ang insert api
      }

      var sub_id = this.checkout_delivery_date.substring(6, 10)+this.checkout_delivery_date.substring(0, 2)+this.checkout_delivery_date.substring(3, 5);
      if(this.primary_key == null)
      {
        this.order_id = parseInt(sub_id+1);
      }
      else
      {
        this.order_id = parseInt(sub_id+(this.primary_key+1));
      }

      /*console.log("THIS DATA WILL BE SENT TO DATABASE:");
      console.log("=====CHECKOUT======");
      console.log("ORDER_ID:",this.order_id);
      console.log("CUSTOMER_ID:", this.cusId);
      console.log("RECIPIENT_ID:", this.recipient_id);
      console.log("DELIVERY_OPTION:",this.checkout_delivery_type);
      console.log("DELIVERY_DATE:", this.checkout_delivery_date);
      console.log("DELIVERY_ADDRESS:", this.checkout_address);
      console.log("PAYMENT_OPTION:", this.checkout_payment_type);
      console.log("GRAND_TOTAL:",this.grandTotal);
      console.log("ORDER_STATUS:", 1);
      console.log("DATE_ADDED:",this.datetime_now);
      console.log("====ORDER TABLE====");*/


      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order.php?id=&order_id='+ this.order_id +'&customer_id='+this.cusId+'&recipient_id='+this.recipient_id+'&delivery_option='+this.checkout_delivery_type+'&delivery_date='+this.checkout_delivery_date+'&delivery_address='+this.checkout_address+'&payment_option='+this.checkout_payment_type+'&grand_total='+this.grandTotal+'&order_status=1&date_added='+this.datetime_now+'&card_number='+this.cardNumber+'&card_name='+this.cardNumber+'&card_cvv='+this.cvv+'&card_expiration='+this.cardExpiration+'&first_store_logo='+this.stores[0].logo+'&gift='+'&gift_status=').subscribe(data => {
        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        console.log(obj);
      });

      console.log("ORDER ID:",this.order_id);

      for (var a=0;a<this.stores.length;a++){
        console.log("=======STORE=======");
        if(this.stores[a].inCheckout == true){
          var grand_total = this.stores[a].cartTotal+this.stores[a].deliveryFee;
          console.log("======~~~~~~~~======");
          console.log(this.stores[a].storeId);
          console.log(this.stores[a].state);
          console.log(this.stores[a].name);
          console.log(this.stores[a].rating);
          console.log(this.stores[a].logo);
          console.log(this.stores[a].address);
          console.log(this.stores[a].contacts);
          console.log(this.stores[a].openHours);
          console.log(this.stores[a].distance);
          console.log(this.stores[a].speed);
          console.log(this.stores[a].viewCount);
          console.log(this.stores[a].orderCount);
          console.log(this.stores[a].inCheckout);
          console.log(this.stores[a].cartTotal);
          console.log(this.stores[a].deliveryFee);
          this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_store.php?id=&order_id='+ this.order_id +'&storeId='+this.stores[a].storeId+'&state='+this.stores[a].state+'&name='+this.stores[a].name+'&rating='+this.stores[a].rating+'&logo='+this.stores[a].logo+'&address='+this.stores[a].address+'&contacts='+this.stores[a].contacts+'&openHours='+this.stores[a].openHours+'&distance='+this.stores[a].distance+'&speed='+this.stores[a].speed+'&viewCount='+this.stores[a].viewCount+'&orderCount='+this.stores[a].orderCount+'&inCheckout='+this.stores[a].inCheckout+'&cartTotal='+this.stores[a].cartTotal+'&deliveryFee='+this.stores[a].deliveryFee+'&date_added='+this.datetime_now+'&customer_id='+this.cusId+'&grand_total='+grand_total).subscribe(data => {
            var str = JSON.stringify(data);
            var obj = JSON.parse(str);
            console.log(obj);
          });
          console.log("======~~~~~~~~======");

          /*console.log("STORE ID:", this.stores[a].storeId);
          console.log("STORE NAME:", this.stores[a].name);
          console.log("CART:", this.stores[a].cartTotal);
          console.log("DELIVERY FEE:", this.stores[a].deliveryFee);
          console.log("TOTAL:", this.stores[a].cartTotal+this.stores[a].deliveryFee);*/

          console.log("======PRODUCTS======");
          for(var c in this.products) {
            if(this.stores[a].storeId == this.products[c].storeId){
              // console.log("STORE ID:"+this.products[c].storeId+" "+"PRODUCT ID:"+this.products[c].prodId+" | "+this.products[c].name+" | "+"QTY:"+this.products[c].quantity+" | "+"₱"+this.products[c].price );

              console.log("======~~~~~~~~======");
              console.log(this.products[c].prodId);
              console.log(this.products[c].storeId);
              console.log(this.products[c].name);
              console.log(this.products[c].rating);
              console.log(this.products[c].image);
              console.log(this.products[c].vending_image);
              console.log(this.products[c].price);
              console.log(this.products[c].discountedPrice);
              console.log(this.products[c].quantity);
              console.log(this.products[c].quantityPrice);
              console.log(this.products[c].inFavorites);
              console.log(this.products[c].toRemove);
              console.log(this.products[c].removing);
              this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_add_order_product.php?id=&order_id='+ this.order_id +'&prodId='+this.products[c].prodId+'&storeId='+this.products[c].storeId+'&customer_id='+this.cusId+'&name='+this.products[c].name+'&rating='+this.products[c].rating+'&image='+this.products[c].image+'&price='+this.products[c].price+'&discountedPrice='+this.products[c].discountedPrice+'&quantity='+this.products[c].quantity+'&quantityPrice='+this.products[c].quantityPrice+'&inFavorites='+this.products[c].inFavorites+'&toRemove='+this.products[c].toRemove+'&removing='+this.products[c].removing+'&date_added='+this.datetime_now).subscribe(data => {
                var str = JSON.stringify(data);
                var obj = JSON.parse(str);
                console.log(obj);
              });
              console.log("======~~~~~~~~======");


            }
          }
          console.log("======~~~~~~~~======");
        }
        console.log("===================");

      }

      console.log("======DELIVERY======");
      console.log("OPTION:", this.checkout_delivery_type);
      console.log("DATE:", this.checkout_delivery_date);
      console.log("ADDRESS:", this.checkout_address);
      console.log("====================");
      console.log("======PAYMENT======");
      console.log("OPTION:", this.checkout_payment_type);
      console.log("GRAND TOTAL P:",this.grandTotal);
      console.log("===================");
      console.log("DATE ADDED:",this.datetime_now);
      console.log("CHECKED OUT");
      console.log("PRODUCTS:",this.products);
      console.log("STORES:",this.stores);
      console.log("ORDERS:",this.orders_table);
      console.log("===================");
      console.log("SUCCESS!");
      this.navCtrl.push(ConfirmorderPage);


    }


  /*  this.stores.forEach( store => {
      if (store.inCheckout) {

        if (store.state) {
          this.data.updateCheckoutCount(--this.checkoutCount)
        } else {
          this.data.updateAdvancedCount(--this.advancedCount)
        }

        this.products.forEach(prod => {
          if (prod.storeId == store.storeId) {
            this.data.updateCartCount(this.cartCount -= prod.quantity);
            this.data.updateProducts(prod.prodId, prod.quantity = 0, prod.quantityPrice = 0);
          }
        });

        this.data.updateStoreCartTotal(store.storeId, store.cartTotal = 0);
        this.data.updateStoreCheckoutStatus(store.storeId, store.inCheckout = false);
      }
    });

    this.navCtrl.push(ConfirmorderPage);*/

  }
  dateNow(){
    var date = new Date();

    var hour = date.toLocaleTimeString('en-GB');
    var hrs   = parseInt(hour.substring(0, 2), 10),
      mins = hour.substring(3, 5),
      pmam    = 'AM';

    if (hrs == 12) {
      pmam = 'PM';
    } else if (hrs == 0) {
      hrs = 12;
    } else if (hrs > 12) {
      hrs -= 12;
      pmam = 'PM';
    }

    var time2 = hrs + ':' + mins + ' ' + pmam;

    var years = date.getUTCFullYear();
    var months = date.getMonth()+1;
    var month = null;
    if (months < 10) {
      month = "0"+months;
    }

    var days = date.getUTCDate();

    this.datetime_now = month+"/"+days+"/"+years+" - "+time2;
    // console.log(this.datetime_now);

  }

  togglePromos() {
    this.promoShown = !this.promoShown;
    if (!this.promoShown) {
      this.grandTotal += this.totalDiscount;
      this.totalDiscount = 0;
    }
  }

  toggleSelect(val) {
    let promo = this.checkoutPromos.find(p => p.promoId == val)
    if (promo.selected) {
      promo.selected = false;
      this.totalDiscount -= promo.discount;
      this.grandTotal += promo.discount;
    } else {
      promo.selected = true;
      this.totalDiscount += promo.discount;
      this.grandTotal -= promo.discount;
    }
  }

  myFriends(){
    this.http.get("https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_get_all_friends.php?friend_request_from="+this.cusId)
      .subscribe(fl => {
        var str = JSON.stringify(fl);
        var obj = JSON.parse(str);

        if(obj.success == 3){
          console.log("NO RESULT!");
          console.log(obj);
        }else{
          console.log("FRIENDS:",obj);
          this.friend_lists = obj.friend_request;
          // this.friend_lists_count = obj.friend_request.length;
          // this.storage.set('my_friend_lists', obj);

        }

      });
  }

  getAddress(){


    this.storage.get("checkout_customer_id").then( (c_id) => {

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_address.php?customer_id='+c_id)
        .subscribe(data => {

          var str = JSON.stringify(data);
          var obj = JSON.parse(str);
          /*var myJSONObj = JSON.stringify(data["address"]);

          var object = JSON.parse(myJSONObj);
          this.myDeliveryAddress = object;*/

          // console.log(myJSONObj);
          console.log(obj);
          console.log(obj.address);
          this.myDeliveryAddress = obj.address;



        })

    });

    /*this.storage.get('customer_id').then((val) => {

      this.http.get('https://beta.o2o.ph/mobile/O2O_Admin_v2/maopao_address.php?customer_id='+val)
        .subscribe(data => {

          var myJSONObj = JSON.stringify(data["address"]);

          var object = JSON.parse(myJSONObj);
          this.myDeliveryAddress = object;

          // console.log(myJSONObj);
          console.log(data);


        })

    });*/

  }

  addAddress() {
    this.storage.set('address_id', null);
    this.storage.set('address_type', null);
    this.storage.set('landmark', null);

    this.navCtrl.push(AddressPage);
  }


}
