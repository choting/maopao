import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SalePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sale',
  templateUrl: 'sale.html',
})
export class SalePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalePage');
  }


  sales = [
    [
      { image: "assets/imgs/sale/sale_10.jpg" },
      { image: "assets/imgs/sale/sale_20.jpg" }
    ],
    [
      { image: "assets/imgs/sale/sale_30.jpg" },
      { image: "assets/imgs/sale/sale_50.jpg" }
    ],
    [
      { image: "assets/imgs/sale/sale_may.jpg" },
      { image: "assets/imgs/sale/sale_summer.jpg" }
    ],
  ];

}
