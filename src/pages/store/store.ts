import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductPage } from '../product/product';
import { ReviewsPage } from '../reviews/reviews';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the StorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store',
  templateUrl: 'store.html',
})
export class StorePage implements OnInit {

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  product:any = null;
  store:any = null;

  filterState = "options";
  fs = false;

  none = "primary";
  rating = "neutral-grey";
  price = "neutral-grey";

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);

    this.store = this.stores.find(s => s.storeId == this.navParams.get("storeId"));

  }

  addToCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, ++product.quantity, (product.quantityPrice += product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal += product.price));
    this.data.updateCartCount(++this.cartCount)
  }

  removeFromCart(id) {
    var product = this.products.find(p => p.prodId == id);
    this.data.updateProducts(id, --product.quantity, (product.quantityPrice -= product.price));
    var store = this.stores.find(store => store.storeId == product.storeId);
    this.data.updateStoreCartTotal(store.storeId, (store.cartTotal -= product.price));
    if ((store.cartTotal == 0) && (store.inCheckout == true)) { 
      this.data.updateStoreCheckoutStatus(store.storeId, (store.inCheckout = false));
      if (store.state) {
        this.data.updateCheckoutCount(--this.checkoutCount);
      } else {
        this.data.updateAdvancedCount(--this.advancedCount);
      } 
    }
    this.data.updateCartCount(--this.cartCount);
  }

  goToProduct(id) {
    this.navCtrl.push(ProductPage, {
      prodId: id
    });
  }

  goToReviews() {
    this.navCtrl.push(ReviewsPage);
  }

  toggleFilter() {
    this.fs = !this.fs;
    if (this.fs) {
      this.filterState = "close";
    }  else {
      this.filterState = "options";
    }
  }

  applyFilter(val: any) {
    if(val == "none") {
      this.none = "primary";  
      this.price = this.rating = "neutral-grey";
    } else if (val == "rating") {
      this.price = this.none = "neutral-grey";  
      this.rating = "primary";
    } else if (val == "price") {
      this.rating = this.none = "neutral-grey";  
      this.price = "primary";
    }
  }

}
