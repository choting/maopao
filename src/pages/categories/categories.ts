import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataserviceProvider } from '../../providers/dataservice/dataservice';

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage implements OnInit {

  stores = [];
  products = [];
  cartCount = 0;
  checkoutCount = 0;
  advancedCount = 0;

  none = "primary";
  rating = "neutral-grey";
  price = "neutral-grey";

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }

  ngOnInit() {
    this.data.currentStores.subscribe(val => this.stores = val);
    this.data.currentProducts.subscribe(val => this.products = val);
    this.data.currentCartCount.subscribe(val => this.cartCount = val);
    this.data.currentCheckoutCount.subscribe(val => this.checkoutCount = val);
    this.data.currentAdvancedCount.subscribe(val => this.advancedCount = val);
  }

  categories = [
    [
      { id: 1,    name: "Beef",         image_1: "assets/imgs/categories/beef_icon.png",          image_2: "assets/imgs/categories/beef_icon_s.png",        selected: false},
      { id: 2,    name: "Dimsum",       image_1: "assets/imgs/categories/dimsum_icon.png",        image_2: "assets/imgs/categories/dimsum_icon_s.png",      selected: false},
      { id: 3,    name: "Coffee",       image_1: "assets/imgs/categories/coffee_icon.png",        image_2: "assets/imgs/categories/coffee_icon_s.png",        selected: false},
      { id: 4,    name: "Chicken",      image_1: "assets/imgs/categories/chicken_icon.png",       image_2: "assets/imgs/categories/chicken_icon_s.png",       selected: false}
    ],
    [
      { id: 5,    name: "Duck",         image_1: "assets/imgs/categories/duck_icon.png",          image_2: "assets/imgs/categories/duck_icon_s.png",          selected: false},
      { id: 6,    name: "Beef",         image_1: "assets/imgs/categories/eggs_icon.png",          image_2: "assets/imgs/categories/eggs_icon_s.png",          selected: false},
      { id: 7,    name: "Fried",        image_1: "assets/imgs/categories/fried_icon.png",         image_2: "assets/imgs/categories/fried_icon_s.png",         selected: false},
      { id: 8,    name: "Fruits",       image_1: "assets/imgs/categories/fruits_icon.png",        image_2: "assets/imgs/categories/fruits_icon_s.png",        selected: false}
    ],
    [
      { id: 9,    name: "Hot Pot",      image_1: "assets/imgs/categories/hotpot_icon.png",        image_2: "assets/imgs/categories/hotpot_icon_s.png",        selected: false},
      { id: 10,   name: "Juice",        image_1: "assets/imgs/categories/juice_icon.png",         image_2: "assets/imgs/categories/juice_icon_s.png",         selected: false},
      { id: 11,   name: "Liquor",       image_1: "assets/imgs/categories/liquor_icon.png",        image_2: "assets/imgs/categories/liquor_icon_s.png",        selected: false},
      { id: 12,   name: "Noodles",      image_1: "assets/imgs/categories/noodles_icon.png",       image_2: "assets/imgs/categories/noodles_icon_s.png",       selected: false}
    ],
    [
      { id: 13,   name: "Pork",         image_1: "assets/imgs/categories/pork_icon.png",          image_2: "assets/imgs/categories/pork_icon_s.png",          selected: false},
      { id: 14,   name: "Rice Meal",    image_1: "assets/imgs/categories/ricemeal_icon.png",      image_2: "assets/imgs/categories/ricemeal_icon_s.png",      selected: false},
      { id: 15,   name: "Sea Food",     image_1: "assets/imgs/categories/seafood_icon.png",       image_2: "assets/imgs/categories/seafood_icon_s.png",       selected: false},
      { id: 16,   name: "Sio Pao",      image_1: "assets/imgs/categories/siopao_icon.png",        image_2: "assets/imgs/categories/siopao_icon_s.png",        selected: false}
    ],
    [
      { id: 17,   name: "Soda",         image_1: "assets/imgs/categories/soda_icon.png",          image_2: "assets/imgs/categories/soda_icon_s.png",          selected: false},
      { id: 18,   name: "Soup",         image_1: "assets/imgs/categories/soup_icon.png",          image_2: "assets/imgs/categories/soup_icon_s.png",          selected: false},
      { id: 19,   name: "Tea",          image_1: "assets/imgs/categories/tea_icon.png",           image_2: "assets/imgs/categories/tea_icon_s.png",           selected: false},
      { id: 20,   name: "Vegetables",   image_1: "assets/imgs/categories/vegetables_icon.png",    image_2: "assets/imgs/categories/vegetables_icon_s.png",    selected: false}
    ]
  ]

  applyFilter(val: any) {
    if(val == "none") {
      this.none = "primary";  
      this.price = this.rating = "neutral-grey";
    } else if (val == "rating") {
      this.price = this.none = "neutral-grey";  
      this.rating = "primary";
    } else if (val == "price") {
      this.rating = this.none = "neutral-grey";  
      this.price = "primary";
    }
  }

  selectingCategories(id) {
    var row = this.categories.find(row => row.some(c => c.id == id));
    var category = row.find(c => c.id == id);

    if (category.selected) {
      category.selected = false;
    } else {
      category.selected = true;
    }
  }

}
