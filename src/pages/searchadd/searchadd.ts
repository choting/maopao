import { Component, ViewChild, ElementRef, OnInit  } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";
import {DataserviceProvider} from "../../providers/dataservice/dataservice";
import {Geolocation} from "@ionic-native/geolocation";
import {AddressPage} from "../address/address";

/**
 * Generated class for the SearchaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searchadd',
  templateUrl: 'searchadd.html',
})
export class SearchaddPage {


  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker: any;

  currentLatitude: any;
  currentLongitude: any;
  latitude: any;
  longitude: any;
  address:any;


  isMapLocEnabled: boolean = false;
  isMapEnabled: boolean = false;
  isNoAddress:boolean = true;
  isListenerOn: boolean = false;
  searchQuery:any;
  searches:any;
  isSearchEnabled:boolean = true;
  search_input:any;
  search_result:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataserviceProvider, private storage: Storage, private http: HttpClient, private geo: Geolocation) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchaddPage');

    setTimeout(() => {
      this.storage.get('address_id').then((val) => {
        console.log('Your address id is', val);
      })

      this.storage.get('mobile').then((val) => {
        console.log('Your mobile is', val);
      });

      this.storage.get('firstname').then((val) => {
        console.log('Your firstname is ', val);
      })

      this.storage.get('lastname').then((val) => {
        console.log('Your lastname is ', val);
      })

      this.storage.get('address_type').then((val) => {
        console.log('Your address_type is ', val);
      })

      this.storage.get('landmark').then((val) => {
        console.log('Your landmark is ', val);
      })

    }, 2500);
  }

  onLocate() {
    this.isMapEnabled = true;
    this.isMapLocEnabled = false;
    this.isNoAddress = false;
    this.isSearchEnabled = false;
    // google.maps.event.clearListeners(this.map, 'dragend');


    this.storage.get('myLat').then((val) => {
      console.log('Your lat is', val);
      this.latitude = val;

      this.storage.get('myLng').then((val) => {
        console.log('Your lng is', val);
        this.longitude = val;

        let latLng = new google.maps.LatLng(this.latitude, this.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng
        });

        var geocoder = new google.maps.Geocoder;



        var latStr = JSON.stringify(this.latitude);
        console.log(latStr);
        var lngStr = JSON.stringify(this.longitude);
        console.log(lngStr);
        var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};

        geocoder.geocode({'location': latlng}, this.geoResults);

      });
    });
  }
  public geoResults = (results) => {


    if (results[0]) {
      console.log(results[0].formatted_address);
      this.address = results[0].formatted_address;
      this.searchQuery = this.address;
    } else {
      console.log('No results found');
    }
  }


  search() {
    this.isMapEnabled = false;
    this.isMapLocEnabled = false;
    this.isNoAddress = true;
    this.isSearchEnabled = true;
    this.searchQuery = "";
  }


  public dragIn = () => {
    this.address = "Loading..."
    console.log(this.address);
  }

  public dragEnd = () => { // <-- note syntax here
    // alert(this.latitude+","+this.longitude);
    var curMap = this.map;

    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lat()));
    console.log("CENTER OF MAP:", JSON.stringify(curMap.getCenter().lng()));

    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyDgzyjliX-k2OwpJapcmoKPM2sK77IWNF0&address=" + curMap.getCenter().lat()+","+curMap.getCenter().lng())
      .subscribe(data => {
        var result = JSON.parse(JSON.stringify(data));
        this.address = result.results[0].formatted_address;
        this.searchQuery = this.address;


        /*var latStr = JSON.stringify(curMap.getCenter().lat());
        var lngStr = JSON.stringify(curMap.getCenter().lng());

        var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};
        this.marker = new google.maps.Marker({
          map: curMap,
          position: latlng
        });*/

        // alert(this.address);
      })
  }

  onSelectArea() {
    this.isMapEnabled = true;
    this.isMapLocEnabled = true;
    this.isNoAddress = false;
    this.isSearchEnabled = false;


    this.storage.get('myLat').then((val) => {
      console.log('Your lat is', val);
      this.latitude = val;
      this.storage.get('myLng').then((val) => {
        console.log('Your lng is', val);
        this.longitude = val;


        var geocoder = new google.maps.Geocoder;
        var latStr = JSON.stringify(this.latitude);
        console.log(latStr);
        console.log(JSON.stringify(this.latitude));

        var lngStr = JSON.stringify(this.longitude);
        console.log(lngStr);
        console.log(JSON.stringify(this.longitude));


        var latlng = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};
        geocoder.geocode({'location': latlng}, this.geoResults);



        let latLng = new google.maps.LatLng(this.latitude, this.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.map.addListener('center_changed', this.dragIn);
        this.map.addListener('dragstart', this.dragIn);
        this.map.addListener('dragend', this.dragEnd);




      })
    })
  }


  goToAddress(val) {

    let currentIndex = this.navCtrl.getActive().index;
    console.log("current index:",currentIndex);

    this.navCtrl.push(AddressPage, {
      address: val
    })
      .then(() => {
      this.navCtrl.remove(currentIndex);

      // this.navCtrl.remove(currentIndex+1);
    });

  }

  public displayPred = (predictions, status) => { // <-- note syntax here

    this.searches = predictions;
    this.search_result = "";
    console.log(this.searches);

    if (status != google.maps.places.PlacesServiceStatus.OK) {
      // alert(status);
      // this.search_result = "No result.";
      return;
    }

    // console.log(JSON.stringify(predictions[0].description));
    // console.log(predictions[1].description);

    predictions.forEach(function (prediction) {

    });

  }

  searching(ev) {
    this.isMapEnabled = false;

    if(ev.target.value == null || ev.target.value == null || this.searchQuery == null || this.searchQuery == ""){
      this.search_input = "asdf";
      this.searches = null;
      this.search_result = "No result.";
    }else {
      this.searches = null;
      this.search_result = "Searching...";



      setTimeout(() => {
        this.search_input = ev.target.value;


        var service = new google.maps.places.AutocompleteService();
        var request = {
          input: this.search_input,
          componentRestrictions: {country: 'ph'},
        };

        service.getPlacePredictions(request, this.displayPred);
      }, 4000);




    }

    // console.log(ev.target.value)


  }

  searchClicked(val){
    console.log(val);
    this.isMapEnabled = true;
    this.search_result = val;
    this.searchQuery = val;
    this.searches = null;


    this.http.get("http://maps.google.com/maps/api/geocode/json??key=AIzaSyCrrmB6NAEi1J5GoON1JlseJBZhu5Ssk7Y&address="+this.search_result)
      .subscribe(data => {
        console.log(data);

        var str = JSON.stringify(data);
        var obj = JSON.parse(str);
        var lat = obj.results[0].geometry.location.lat;
        var lng = obj.results[0].geometry.location.lng;

        if(lat != null && lng != null)
        {
          let latLng = new google.maps.LatLng(lat, lng);
          let mapOptions = {
            center: latLng,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }

          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

          var marker = new google.maps.Marker({
            map: this.map,
            position: latLng
          });
        }

      });


  }


}
