import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchaddPage } from './searchadd';

@NgModule({
  declarations: [
    SearchaddPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchaddPage),
  ],
})
export class SearchaddPageModule {}
