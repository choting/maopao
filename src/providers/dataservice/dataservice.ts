import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BootstrapOptions } from '@angular/core/src/application_ref';

/*
  Generated class for the DataserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataserviceProvider {

  //store list
  stores = [
    { storeId: 0, state: true,  name: "All Fresh Mart",           rating: 4.9, logo: "assets/imgs/store_logos/1.jpg", address: "123, Juan Luna, Binondo Manila",      contacts: "837-3746", openHours: "10:00 - 10:00", distance: 1,   speed: 30, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 50 },
    { storeId: 1, state: false, name: "Chan Ren Fang Restaurant", rating: 3.8, logo: "assets/imgs/store_logos/2.jpg", address: "1, Escolta, Binondo Manila",          contacts: "736-3464", openHours: "09:00 - 10:00", distance: 1.8, speed: 45, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 40 },
    { storeId: 2, state: true,  name: "Chicky",                   rating: 3.2, logo: "assets/imgs/store_logos/3.jpg", address: "2, Dasmarinas Luna, Binondo Manila",  contacts: "827-2533", openHours: "08:00 - 10:00", distance: 2.1, speed: 50, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 30 },
    { storeId: 3, state: false, name: "Fruit Wishes",             rating: 4.1, logo: "assets/imgs/store_logos/4.png", address: "3, Divisoria, Manila",                contacts: "193-7363", openHours: "10:00 - 10:00", distance: 3.6, speed: 60, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 10 },
    { storeId: 4, state: true,  name: "Hui Wei",                  rating: 2.6, logo: "assets/imgs/store_logos/5.jpg", address: "321, Binondo, Binondo Manila",        contacts: "847-6637", openHours: "10:00 - 10:00", distance: 8,   speed: 30, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 20 },
    { storeId: 5, state: false, name: "Soul Salad",               rating: 3.3, logo: "assets/imgs/store_logos/6.jpg", address: "76, Rpapa, Manila",                   contacts: "374-6374", openHours: "10:00 - 10:00", distance: 7,   speed: 60, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 30 },
    { storeId: 6, state: true,  name: "Zhou Ji Hei Ya",           rating: 1.2, logo: "assets/imgs/store_logos/7.png", address: "85, Juan Luna, Manila",               contacts: "927-3643", openHours: "10:00 - 10:00", distance: 4,   speed: 30, viewCount: 0, orderCount: 0, inCheckout: false, cartTotal: 0, deliveryFee: 40 }
  ]

  //product list
  products = [
    { prodId: 0,   storeId: 0,  name: "Steamed Hot Buns",                   rating: 4.8, image: "assets/imgs/products/9.png",                                                                                       price: 45,                             quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 1,   storeId: 1,  name: "Green Veggies",                      rating: 4.4, image: "assets/imgs/products/15.png",                                                                                      price: 58,                             quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 2,   storeId: 2,  name: "Hot Pot Noodles",                    rating: 4.5, image: "assets/imgs/products/8.png",                                                                                       price: 534,                            quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 3,   storeId: 3,  name: "Fried Egg",                          rating: 5,   image: "assets/imgs/products/10.jpg",                                                                                      price: 34,                             quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 4,   storeId: 4,  name: "Sausages",                           rating: 2.8, image: "assets/imgs/products/12.jpeg",                                                                                     price: 486,                            quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 5,   storeId: 5,  name: "Squid Balls",                        rating: 4.5, image: "assets/imgs/products/13.jpg",                                                                                      price: 736.25,                         quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 6,   storeId: 6,  name: "Fermented Meat",                     rating: 3.7, image: "assets/imgs/products/11.jpg",                                                                                      price: 100,    discountedPrice: 80,    quantity: 0, quantityPrice: 0, inFavorites: true,   toRemove: false, removing: false },
    { prodId: 7,   storeId: 0,  name: "Black Treats",                       rating: 2.4, image: "assets/imgs/products/14.png",                                                                                      price: 39,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 8,   storeId: 1,  name: "Black Noodles",                      rating: 2.6, image: "assets/imgs/products/16.png",                                                                                      price: 55,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 9,   storeId: 2,  name: "Spicy Sauce",                        rating: 4.4, image: "assets/imgs/products/1.jpg",                                                                                       price: 10,     discountedPrice: 5.50,  quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 10,  storeId: 3,  name: "Jay Milk",                           rating: 5,   image: "assets/imgs/products/6.jpg",                                                                                       price: 17,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 11,  storeId: 4,  name: "Tasty Treat",                        rating: 3.7, image: "assets/imgs/products/4.jpg",                                                                                       price: 89,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 12,  storeId: 5,  name: "Sticky Meat",                        rating: 4.9, image: "assets/imgs/products/5.jpg",                                                                                       price: 92,     discountedPrice: 65,    quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 13,  storeId: 6,  name: "Crunchy Nuts",                       rating: 1.5, image: "assets/imgs/products/2.jpg",                                                                                       price: 36,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 14,  storeId: 0,  name: "Panda Milk",                         rating: 4.5, image: "assets/imgs/products/3.jpg",                                                                                       price: 72,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 15,  storeId: 0,  name: "Milk Tea",                           rating: 4.6, image: "assets/imgs/vending_machine/d1_normal.jpg",              vending_image: "assets/imgs/vending_machine/d1.png",     price: 44.50,                          quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 16,  storeId: 0,  name: "Green Tea",                          rating: 4.7, image: "assets/imgs/vending_machine/d2_normal.jpg",              vending_image: "assets/imgs/vending_machine/d2.png",     price: 73.25,                          quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 17,  storeId: 0,  name: "Absolut Vodka",                      rating: 4.8, image: "assets/imgs/vending_machine/d3_normal.jpg",              vending_image: "assets/imgs/vending_machine/d3.png",     price: 55.50,                          quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 18,  storeId: 0,  name: "Iced Tea",                           rating: 4.9, image: "assets/imgs/vending_machine/d4_normal.jpg",              vending_image: "assets/imgs/vending_machine/d4.png",     price: 42,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 19,  storeId: 0,  name: "Lemon Juice",                        rating: 4.1, image: "assets/imgs/vending_machine/d5_normal.jpg",              vending_image: "assets/imgs/vending_machine/d5.png",     price: 28,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 20,  storeId: 0,  name: "Blue Tea",                           rating: 1.2, image: "assets/imgs/vending_machine/d6_normal.jpg",              vending_image: "assets/imgs/vending_machine/d6.png",     price: 38,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 21,  storeId: 0,  name: "Orange Juice",                       rating: 2.3, image: "assets/imgs/vending_machine/d7_normal.jpg",              vending_image: "assets/imgs/vending_machine/d7.png",     price: 48,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 22,  storeId: 0,  name: "Mogu Mogu",                          rating: 3.4, image: "assets/imgs/vending_machine/d8_normal.jpg",              vending_image: "assets/imgs/vending_machine/d8.png",     price: 20,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 23,  storeId: 0,  name: "Coke",                               rating: 4.5, image: "assets/imgs/vending_machine/d9_normal.jpg",              vending_image: "assets/imgs/vending_machine/d9.png",     price: 16,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 24,  storeId: 0,  name: "Vita Milk",                          rating: 3.6, image: "assets/imgs/vending_machine/d10_normal.jpg",             vending_image: "assets/imgs/vending_machine/d10.png",    price: 73,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 25,  storeId: 0,  name: "Minute Maid Orange",                 rating: 4.7, image: "assets/imgs/vending_machine/d11_normal.jpg",             vending_image: "assets/imgs/vending_machine/d11.png",    price: 28,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 26,  storeId: 0,  name: "Frappy",                             rating: 3.8, image: "assets/imgs/vending_machine/d12_normal.jpg",             vending_image: "assets/imgs/vending_machine/d12.png",    price: 47,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 27,  storeId: 0,  name: "Choco Frappe",                       rating: 2.9, image: "assets/imgs/vending_machine/d13_normal.jpg",             vending_image: "assets/imgs/vending_machine/d13.png",    price: 57,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 28,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/vending_machine/d14_normal.jpg",             vending_image: "assets/imgs/vending_machine/d14.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 29,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g1.jpeg",                             grocery_image: "assets/imgs/grocery/grocery_g1.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 30,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g2.jpg",                             grocery_image: "assets/imgs/grocery/grocery_g2.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 31,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g3.png",                             grocery_image: "assets/imgs/grocery/grocery_g3.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 32,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g4.jpg",                             grocery_image: "assets/imgs/grocery/grocery_g4.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 33,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g5.png",                             grocery_image: "assets/imgs/grocery/grocery_g5.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 34,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g6.jpg",                             grocery_image: "assets/imgs/grocery/grocery_g6.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 35,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g7.png",                             grocery_image: "assets/imgs/grocery/grocery_g7.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 36,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g8.jpg",                             grocery_image: "assets/imgs/grocery/grocery_g8.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 37,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g9.jpg",                             grocery_image: "assets/imgs/grocery/grocery_g9.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 38,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g10.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g10.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 39,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g11.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g11.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 40,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g12.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g12.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 41,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g13.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g13.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 42,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g14.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g14.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 43,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g15.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g15.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 44,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g16.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g16.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 45,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g17.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g17.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 46,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g18.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g18.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 47,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g19.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g19.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 48,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g20.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g20.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 49,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g21.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g21.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 50,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g22.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g22.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 51,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g23.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g23.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 52,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g24.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g24.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 53,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g25.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g25.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 54,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g26.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g26.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 55,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g27.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g27.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 56,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g28.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g28.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 57,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g29.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g29.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
    { prodId: 58,  storeId: 0,  name: "Wine",                               rating: 1.5, image: "assets/imgs/grocery/g30.jpg",                            grocery_image: "assets/imgs/grocery/grocery_g30.png",    price: 53,                             quantity: 0, quantityPrice: 0, inFavorites: false,  toRemove: false, removing: false },
  ]

  //tab cart count
  cartCount     = 0;
  checkoutCount = 0;
  advancedCount = 0;

  // recepient list for sending delivery
  friends = [
    [
      { id: 1,  name: "Mary Rose",      image: "assets/imgs/friends/babe_1.jpg", set: false },
      { id: 2,  name: "Mei Fuyumine",   image: "assets/imgs/friends/babe_2.jpg", set: false },
      { id: 3,  name: "Jenny Lee",      image: "assets/imgs/friends/babe_3.jpg", set: false }
    ],
    [
      { id: 4,  name: "Rebecca Lin",    image: "assets/imgs/friends/babe_4.jpg", set: false },
      { id: 5,  name: "Emma Woo",       image: "assets/imgs/friends/babe_5.jpg", set: false },
      { id: 6,  name: "Mia Monroe",     image: "assets/imgs/friends/babe_6.jpg", set: false }
    ],
    [
      { id: 7,  name: "Abby Moon",      image: "assets/imgs/friends/babe_7.jpg", set: false },
      { id: 8,  name: "Sophia Niervo",  image: "assets/imgs/friends/babe_8.jpg", set: false },
      { id: 9,  name: "Michaela Ava",   image: "assets/imgs/friends/babe_9.jpg", set: false }
    ],
    [
      { id: 10,  name: "Jasmine Tan",    image: "assets/imgs/friends/babe_10.jpg", set: false },
      { id: 11, name: "Catherine Abe",  image: "assets/imgs/friends/babe_11.jpg", set: false },
      { id: 12, name: "Jessica Sun",    image: "assets/imgs/friends/babe_12.jpg", set: false }
    ],
    [
      { id: 13, name: "Cecilia Mena",   image: "assets/imgs/friends/babe_13.jpg", set: false },
      { id: 14, name: "Elsa Green",     image: "assets/imgs/friends/babe_14.jpg", set: false },
      { id: 15, name: "Demmy Jones",    image: "assets/imgs/friends/babe_15.jpg", set: false }
    ],
    [
      { id: 16, name: "Priscilla Chan", image: "assets/imgs/friends/babe_16.jpg", set: false }
    ]
  ]

  user = {
    id: 18, name: "Michael John C. Ponce", image: "assets/imgs/user/avatar.jpg"
  }

  private userSource = new BehaviorSubject<any>(this.user);
  currentUser = this.userSource.asObservable();

  /* delivery states
    0 = pending gift to friend
    1 = gift to friend in process
    2 = gift to friend in transit
    3 = gift to friend delivered
    4 = pending gift from friend
    5 = gift from friend in process
    6 = gift from friend in transit
    7 = gift from friend delivered
    8 = order is processing in store
    9 = order is in transit
    10 = order has been delivered
  */

  //orders list
  orders = [
    {
      // order to friend, pending
      orderId: 837463846374,  date: "22/05/2018 - 6:18 PM",   deliveryState: 0,   senderId: 18,   recepientId: 15,  storeId: 0,   eta: "22/05/2018 - 6:48 PM",   addressId: 18,   deliveryOptions: "asap",    paymentOptions: "Credit card",    cardNumber: "**** **** **** 4986",                    products: [ { prodId: 0, quantity: 4 }, { prodId: 7, quantity: 4 } ]
    },
    {
      // order from friend, pending
      orderId: 837463846375, unseen: true,  date: "23/05/2018 - 12:15 PM",  deliveryState: 4,   senderId: 12,   recepientId: 18,     storeId: 1,   eta: "22/05/2018 - 1:00 PM",   addressId: 3,   deliveryOptions: "advanced",    paymentOptions: "Credit card",      cardNumber: "**** **** **** 4986",   products: [ { prodId: 0, quantity: 3 }, { prodId: 7, quantity: 5 } ]
    },
    {
      // order to friend, accepted
      orderId: 837463846376,  date: "24/05/2018 - 3:37 PM",   deliveryState: 2,   senderId: 18,   recepientId: 8,     storeId: 2,   eta: "22/05/2018 - 4:27 PM",   addressId: 9,   deliveryOptions: "asap",    paymentOptions: "Credit card",      cardNumber: "**** **** **** 4986",   products: [ { prodId: 1, quantity: 9 }, { prodId: 8, quantity: 6 } ]
    },
    {
      // order from friend, accepted
      orderId: 837463846377,  date: "25/05/2018 - 10:09 AM",  deliveryState: 7,   senderId: 4,   recepientId: 18,    storeId: 3,   eta: "22/05/2018 - 11:09 AM",   addressId: 18,   deliveryOptions: "asap",    paymentOptions: "Credit card",     cardNumber: "**** **** **** 4986",  products: [ { prodId: 2, quantity: 7 }, { prodId: 9, quantity: 12 } ]
    },
    {
      // order to self
      orderId: 837463846378,  date: "26/05/2018 - 11:20 AM",  deliveryState: 8,   senderId: null,   recepientId: null,    storeId: 4,   eta: "22/05/2018 - 11:50 AM",   addressId: 18,   deliveryOptions: "asap",    paymentOptions: "Cash on Delivery",     cardNumber: null,  products: [ { prodId: 3, quantity: 1 }, { prodId: 10, quantity: 4 } ]
    }
  ];

  private ordersSource = new BehaviorSubject<any>(this.orders);
  currentOrders = this.ordersSource.asObservable();

  updateOrders(arr: any) {
    this.ordersSource.next(arr)
  }

  myAddress = [
    { id: 18, value: "2301 World Trade Exchange bldg, Juan Luna, Binondo Manila" },
    { id: 18, value: "193 Munich street, barangay La Yola, Bacolod city" },
    { id: 18, value: "17 Milan street, barangay Di Matagpuan, Cebu city" },
  ]

  theirAddress = [
    { id: 1, value: "737 Building B, Binondo Manila" },
    { id: 2, value: "846 Reyes street, barangay Regalia, Makati city" },
    { id: 3, value: "29 Mundo street, barangay Juan, Marikina city" },
    { id: 4, value: "847 Mali street, barangay Juan, Pasay city" },
    { id: 5, value: "2 Doon street, barangay Juan, Manila city" },
    { id: 6, value: "6 Here street, barangay Juan, Quezon city" },
    { id: 7, value: "37 For street, barangay Juan, Pasig city" },
    { id: 8, value: "27 Nowo street, barangay Juan, Mandaluyong city" },
    { id: 9, value: "74 Kailan  street, barangay Juan, Caloocan city" },
    { id: 10, value: "7 Ewan street, barangay Juan, Pasay city" },
    { id: 11, value: "8 Mundo street, barangay Juan, Marikina city" },
    { id: 12, value: "2 norway street, barangay Juan, Quezon city" },
    { id: 13, value: "29 hire street, barangay Juan, Manila city" },
    { id: 14, value: "3 mant street, barangay Juan, Manila city" },
    { id: 15, value: "6 wanson street, barangay Juan, Manila city" },
    { id: 16, value: "15 shiba street, barangay Juan, Pasig city" }
  ]

  storeState: boolean = true;
  private storeStateSource = new BehaviorSubject<boolean>(this.storeState);
  currentStoreState = this.storeStateSource.asObservable();

  updateStoreState(val: boolean) {
    this.storeStateSource.next(val)
  }

  storePromos = [
    { promoId: 1,   type: 1,   storeId: 0,   discount: 300,    cost: 200,    period: "Jan 4, 2018 - Dec 18, 2018", selected: false, bought: false },
    { promoId: 2,   type: 2,   storeId: 1,   discount: 500,    cost: 200,    period: "Jan 1, 2019 - Dec 31, 2019", selected: false, bought: false },
    { promoId: 3,   type: 1,   storeId: 2,   discount: 250,    cost: 200,    period: "June 5, 2018 - Oct 20, 2018", selected: false, bought: false }
  ]

  private storePromosSource = new BehaviorSubject<any>(this.storePromos);
  currentStorePromos = this.storePromosSource.asObservable();

  updatePromos(id: number, val: boolean){
    let promo = this.storePromos.find(p => p.promoId == id);
    let promoSource = new BehaviorSubject<boolean>(promo.bought);
    promoSource.next(val);

    let cp = [];

    // update promo list in checkout page
    // make storestate universal then iterate through stores and promos to list available in checkout page
    this.stores.forEach(s => {
      this.storePromos.forEach(p => {
        if (s.storeId == p.storeId && s.state == this.storeState && s.inCheckout) {
          p.selected  = false;
          cp.push(p)
        }
      })
    });

    this.checkoutPromosSource.next(cp);
  }

  checkoutPromos = [];

  private checkoutPromosSource = new BehaviorSubject<any>(this.checkoutPromos);
  currentCheckoutPromos = this.checkoutPromosSource.asObservable();

  myPoints:number = 18029;

  private myPointsSource = new BehaviorSubject<number>(this.myPoints);
  myCurrentPoints = this.myPointsSource.asObservable();

  private myAddressSource = new BehaviorSubject<any>(this.myAddress);
  myCurrentAddress = this.myAddressSource.asObservable();

  private theirAddressSource = new BehaviorSubject<any>(this.theirAddress);
  theirCurrentAddress = this.theirAddressSource.asObservable();

  private storesSource = new BehaviorSubject<any>(this.stores);
  currentStores = this.storesSource.asObservable();

  updateStoreCartTotal(id: number, ct: number) {
    var store = this.stores.find(store => store.storeId == id);
    var cartTotalSource = new BehaviorSubject<number>(store.cartTotal);
    cartTotalSource.next(ct);
  }

  updateStoreCheckoutStatus(id: number, cs: boolean) {
    var store = this.stores.find(store => store.storeId == id);
    var checkoutStatus = new BehaviorSubject<boolean>(store.inCheckout);
    checkoutStatus.next(cs);
  }

  private productsSource = new BehaviorSubject<any>(this.products);
  currentProducts = this.productsSource.asObservable();

  updateProducts(id: number, q: number, qp: number) {
    var product = this.products.find(p => p.prodId == id);
    var productQuantitySource = new BehaviorSubject<number>(product.quantity);
    var productQuantityPriceSource = new BehaviorSubject<number>(product.quantityPrice);
    productQuantitySource.next(q);
    productQuantityPriceSource.next(qp);
  }

  updateFavorites(i: number, f: boolean) {
    var favoritesSource = new BehaviorSubject<boolean>(this.products[i].inFavorites);
    favoritesSource.next(f);
  }

  private cartCountSource = new BehaviorSubject<number>(this.cartCount);
  currentCartCount = this.cartCountSource.asObservable();

  updateCartCount(n: number) {
    this.cartCountSource.next(n)
  }

  private checkoutCountSource = new BehaviorSubject<number>(this.checkoutCount);
  currentCheckoutCount = this.checkoutCountSource.asObservable();

  updateCheckoutCount(n: number) {
    this.checkoutCountSource.next(n);
  }

  private advancedCountSource = new BehaviorSubject<number>(this.advancedCount);
  currentAdvancedCount = this.advancedCountSource.asObservable();

  updateAdvancedCount(n: number) {
    this.advancedCountSource.next(n)
  }

  private friendsSource = new BehaviorSubject<any>(this.friends);
  currentFriends = this.friendsSource.asObservable();

  updateFriends(id: number, set: boolean) {

    var row = this.friends.find(row => row.some(friend => friend.id == id));
    var friend = row.find(f => f.id == id);

    var setSource = new BehaviorSubject<boolean>(friend.set);
    setSource.next(set);

  }

  friendSelected:number = null;

  private friendSelectedSource = new BehaviorSubject<number>(this.friendSelected);
  currentFriendSelected = this.friendSelectedSource.asObservable();

  setCurrentFriendSelected(id: number) {
    this.friendSelectedSource.next(id);
  }

  loggedIn: boolean = false;

  private loggedInSource = new BehaviorSubject<boolean>(this.loggedIn);
  currentLoggedIn = this.loggedInSource.asObservable();

  updateLoggedInValue(val: boolean) {
    this.loggedInSource.next(val)
  }

  constructor() {}

}
