import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the NetworkEngineProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkEngineProvider {

  constructor(public http: HttpClient) {
    console.log('Hello NetworkEngineProvider Provider');
  }


  callPost(mobileNumber , pass): Promise<any>{
    console.log("Mobile = "+mobileNumber + "Password = "+ pass);

    let param = {mobile: mobileNumber, password: pass};

    let url = "https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_register_customer.php";

    let request = this.http.post(url, param);


    return request.toPromise()
  }





  /*register(mobile, password) : Promise<any>
  {

    let url = 'https://beta.o2o.ph/mobile/O2O_Admin_v2/mao_register_customer.php';

    let param =  { mobile: mobile , password: password}

    let headers = 'Content-Type: application/json';

    let request = this.http.post(url , param);

    return request.toPromise();

  }*/

}
